MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR += $(realpath ../../src)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR += $(realpath ../../include_priv)
# MOCK_SRCDIR
COMMON_SRCDIR += $(realpath ../common/)
# MOCK_INCDIR = $(realpath ../common/)
INCDIR += $(realpath ../common_includes/)

HEADERS = $(wildcard $(addsuffix /*.h,$(INCDIR)))
SOURCES = $(wildcard $(SRCDIR)/*.c)
SOURCES += $(wildcard $(COMMON_SRCDIR)/*.c)
SOURCES += $(wildcard $(MOCK_SRCDIR)/*.c)

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. \
		  -fkeep-inline-functions -fkeep-static-functions \
		  -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka) -pthread \
		  -D_GNU_SOURCE

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) \
		   -lamxc -lamxp -lamxd -lamxo \
		   -lsahtrace -ldl -lpthread -lcrypt -lamxj -lyajl

ifeq ($(CC_NAME),g++)
    CFLAGS += -std=c++2a
else
	CFLAGS += -std=gnu11
endif