/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "test_datamodel.h"
#include "test_environment.h"
#include "usermanagement_util.h"
#include "dm_usermanagement.h"
#include "passwd_hash.h"
#include "access_role.h"
#include "file_system.h"
#include "common_test_data.h"
#include <amxp/amxp_dir.h>


#define TESTUSER_GROUPID DEFAULT_GROUP_ID

#define NEW_USERNAME "new-user"

#define NEW_SHELL_ALIAS "new-shell"
#define NEW_SHELL_PATH "Users.SupportedShell."NEW_SHELL_ALIAS "."
#define NEW_SHELL_NAME "/bin/true"

#define NEW_ROLE_PATH  "Users.Role.new-role."
#define ACCESS_BUS_ROLE_PATH "Users.Role.5"

#define PASSWORD "password"
#define NEW_PASSWORD "new_password"
#define HASHED_PASSWORD "$algo$salt$hash"
#define NEW_HASHED_PASSWORD "$algo$snewsalt$newhash"

#define ME "dm"

static gid_t testuser_gid = TESTUSER_GROUPID;

static const userdata_t empty_user = {
    .username = NULL,
    .previous_username = NULL,
    .user_id = 0,
    .hashed_password = NULL,
    .password = NULL,
    .home_dir = NULL,
    .shell_path = NULL,
    .role_paths = NULL,
    .primary_group_id = 0,
    .all_group_ids.size = 0,
    .all_group_ids.list = NULL,
    .enable = false,
};

static const userdata_t test_user = {
    .username = "test-user",
    .previous_username = NULL,
    .user_id = 666,
    .password = PASSWORD,
    .hashed_password = NULL,
    .home_dir = "/",
    .role_paths = DEFAULT_ROLE_PATH,
    .shell_path = "/bin/bash",
    .primary_group_id = TESTUSER_GROUPID,
    .all_group_ids.size = 1,
    .all_group_ids.list = &testuser_gid,
    .enable = true,
};

static const user_dm_object_t test_user_dm_object = {
    .m_alias = "testusr",
    .m_index = 8,
    .m_user_id = 666,
    .m_username = "test-user",
    .m_password = PASSWORD,
    .m_hashed_password = NULL,
    .m_group_participation = DEFAULT_GROUP_PATH,
    .m_role_participation = DEFAULT_ROLE_PATH,
    .m_shell = DEFAULT_SHELL_PATH,
    .m_homedir = "/",
    .m_enable = true,
    .m_static_user = false,
    .m_language = PASSWORD_EMPTY_STR,
};

static amxd_object_t* dm_get_user_instance(const char* username) {
    amxd_object_t* users = NULL;
    amxd_object_t* user = NULL;

    when_null(username, exit);
    users = amxd_dm_findf(get_test_datamodel(), "Users.User.");
    when_null_trace(users, exit, INFO, "Users object is NULL");
    user = amxd_object_findf(users, "[" "Username" " == '%s']", username);
    when_null_trace(user, exit, INFO, "Failed to find user with Username = %s", username);
exit:
    return user;
}

static void dm_check_password(const char* username, const char* password_expected, const char* hashed_password_expected) {
    amxd_param_t* password_param = NULL;
    amxd_param_t* hashed_password_param = NULL;
    const char* current_password = NULL;
    const char* current_hashed_password = NULL;

    amxd_object_t* user = dm_get_user_instance(username);

    password_param = amxd_object_get_param_def(user, DM_PASSWD_NAME);
    if(password_param != NULL) {
        current_password = amxc_var_get_const_cstring_t(&password_param->value);
    }
    hashed_password_param = amxd_object_get_param_def(user, get_hashed_password_path());
    if(hashed_password_param != NULL) {
        current_hashed_password = amxc_var_get_const_cstring_t(&hashed_password_param->value);
    }

    assert_string_equal(password_expected, current_password);
    assert_string_equal(hashed_password_expected, current_hashed_password);
}

void test_add_user_with_empty_username(UNUSED void** state) {
    amxd_object_t* p_user_dm_obj = NULL;
    userdata_t expected_test_user = test_user;
    userdata_t expected_del_user = empty_user;
    user_dm_object_t user_dm_object = test_user_dm_object;

    user_dm_object.m_username = "";
    user_dm_object.m_password = PASSWORD;
    user_dm_object.m_hashed_password = PASSWORD_EMPTY_STR;

    setup_default_dm(get_test_datamodel());

    add_user_to_dm(get_test_datamodel(), &user_dm_object, &p_user_dm_obj);
    amxc_var_t* p_event_variant = convert_user_to_amxc_var(&user_dm_object);

    will_return(__wrap_crypt, PASSWORD);
    will_return(__wrap_crypt, HASHED_PASSWORD);

    expected_test_user.username = "testusr";
    expected_test_user.password = PASSWORD_SPECIAL_STR;
    expected_test_user.hashed_password = HASHED_PASSWORD;

    will_return(__wrap_sysconf_add_user, &expected_test_user);
    will_return(__wrap_sysconf_add_user, 0);

    _sync_user_to_system("dm:instance-added", p_event_variant, NULL);

    expected_del_user.username = "testusr";
    expected_del_user.user_id = test_user.user_id;

    will_return(__wrap_sysconf_del_user, &expected_del_user);
    will_return(__wrap_sysconf_del_user, 0);
    amxd_object_delete(&p_user_dm_obj);

    _del_user_from_system("dm:instance-removed", p_event_variant, NULL);

    amxc_var_delete(&p_event_variant);

    teardown_default_dm();
}

void test_add_user_with_default_hashed_password(UNUSED void** state) {
    amxd_object_t* p_user_dm_obj = NULL;
    userdata_t expected_test_user = test_user;
    userdata_t expected_del_user = empty_user;
    user_dm_object_t user_dm_object = test_user_dm_object;

    expected_test_user.password = PASSWORD_SPECIAL_STR;
    expected_test_user.hashed_password = HASHED_PASSWORD;

    user_dm_object.m_password = PASSWORD_EMPTY_STR;
    user_dm_object.m_hashed_password = HASHED_PASSWORD;

    expected_del_user.username = test_user.username;
    expected_del_user.user_id = test_user.user_id;

    setup_default_dm(get_test_datamodel());

    add_user_to_dm(get_test_datamodel(), &user_dm_object, &p_user_dm_obj);
    amxc_var_t* p_event_variant = convert_user_to_amxc_var(&user_dm_object);

    will_return(__wrap_sysconf_add_user, &expected_test_user);
    will_return(__wrap_sysconf_add_user, 0);

    _sync_user_to_system("dm:instance-added", p_event_variant, NULL);

    dm_check_password(test_user.username, PASSWORD_SPECIAL_STR, HASHED_PASSWORD);

    will_return(__wrap_sysconf_del_user, &expected_del_user);
    will_return(__wrap_sysconf_del_user, 0);
    amxd_object_delete(&p_user_dm_obj);

    _del_user_from_system("dm:instance-removed", p_event_variant, NULL);

    amxc_var_delete(&p_event_variant);

    teardown_default_dm();
}

void test_add_user_with_default_clear_password(UNUSED void** state) {
    amxd_object_t* p_user_dm_obj = NULL;
    userdata_t expected_test_user = test_user;
    userdata_t expected_del_user = empty_user;
    user_dm_object_t user_dm_object = test_user_dm_object;

    expected_del_user.username = test_user.username;
    expected_del_user.user_id = test_user.user_id;

    user_dm_object.m_password = PASSWORD;
    user_dm_object.m_hashed_password = PASSWORD_EMPTY_STR;

    setup_default_dm(get_test_datamodel());

    add_user_to_dm(get_test_datamodel(), &user_dm_object, &p_user_dm_obj);
    amxc_var_t* p_event_variant = convert_user_to_amxc_var(&user_dm_object);

    will_return(__wrap_crypt, PASSWORD);
    will_return(__wrap_crypt, HASHED_PASSWORD);

    expected_test_user.password = PASSWORD_SPECIAL_STR;
    expected_test_user.hashed_password = HASHED_PASSWORD;

    will_return(__wrap_sysconf_add_user, &expected_test_user);
    will_return(__wrap_sysconf_add_user, 0);

    _sync_user_to_system("dm:instance-added", p_event_variant, NULL);

    dm_check_password(test_user.username, PASSWORD_SPECIAL_STR, HASHED_PASSWORD);

    will_return(__wrap_sysconf_del_user, &expected_del_user);
    will_return(__wrap_sysconf_del_user, 0);
    amxd_object_delete(&p_user_dm_obj);

    _del_user_from_system("dm:instance-removed", p_event_variant, NULL);

    amxc_var_delete(&p_event_variant);

    teardown_default_dm();
}

void test_add_user_with_group_and_role(UNUSED void** state) {
    amxd_object_t* p_user_dm_obj = NULL;
    userdata_t expected_test_user = test_user;
    userdata_t expected_del_user = empty_user;
    user_dm_object_t user_dm_object = test_user_dm_object;

    expected_del_user.username = test_user.username;
    expected_del_user.user_id = test_user.user_id;

    user_dm_object.m_password = PASSWORD;
    user_dm_object.m_hashed_password = PASSWORD_EMPTY_STR;

    setup_default_dm(get_test_datamodel());
    add_user_to_dm(get_test_datamodel(), &user_dm_object, &p_user_dm_obj);

    amxc_var_t* p_event_variant = convert_user_to_amxc_var(&user_dm_object);

    will_return(__wrap_crypt, PASSWORD);
    will_return(__wrap_crypt, HASHED_PASSWORD);

    expected_test_user.password = PASSWORD_SPECIAL_STR;
    expected_test_user.hashed_password = HASHED_PASSWORD;

    will_return(__wrap_sysconf_add_user, &expected_test_user);
    will_return(__wrap_sysconf_add_user, 0);

    _sync_user_to_system("dm:instance-added", p_event_variant, NULL);

    assert_string_equal("Users.Group.test-grp.", GET_CHAR(amxd_object_get_param_value(p_user_dm_obj, DM_GROUP_PARTICIPATION_NAME), NULL));
    assert_string_equal("Users.Role.test-role.", GET_CHAR(amxd_object_get_param_value(p_user_dm_obj, DM_ROLE_PARTICIPATION_NAME), NULL));

    will_return(__wrap_sysconf_del_user, &expected_del_user);
    will_return(__wrap_sysconf_del_user, 0);
    amxd_object_delete(&p_user_dm_obj);

    _del_user_from_system("dm:instance-removed", p_event_variant, NULL);

    amxc_var_delete(&p_event_variant);

    teardown_default_dm();
}

void test_add_user_with_empty_group_and_role(UNUSED void** state) {
    amxd_object_t* p_user_dm_obj = NULL;
    userdata_t expected_test_user = test_user;
    userdata_t expected_del_user = empty_user;
    user_dm_object_t user_dm_object = test_user_dm_object;
    gid_t nogroup_gid_list[1] = {65534};

    expected_del_user.username = test_user.username;
    expected_del_user.user_id = test_user.user_id;

    user_dm_object.m_password = PASSWORD;
    user_dm_object.m_hashed_password = PASSWORD_EMPTY_STR;
    user_dm_object.m_group_participation = "";
    user_dm_object.m_role_participation = "";

    setup_default_dm(get_test_datamodel());
    add_user_to_dm(get_test_datamodel(), &user_dm_object, &p_user_dm_obj);

    amxc_var_t* p_event_variant = convert_user_to_amxc_var(&user_dm_object);

    will_return(__wrap_crypt, PASSWORD);
    will_return(__wrap_crypt, HASHED_PASSWORD);

    expected_test_user.password = PASSWORD_SPECIAL_STR;
    expected_test_user.hashed_password = HASHED_PASSWORD;
    expected_test_user.primary_group_id = 65534;
    expected_test_user.all_group_ids.size = 1;
    expected_test_user.all_group_ids.list = (gid_t*) &nogroup_gid_list;
    expected_test_user.role_paths = "";

    will_return(__wrap_sysconf_add_user, &expected_test_user);
    will_return(__wrap_sysconf_add_user, 0);

    _sync_user_to_system("dm:instance-added", p_event_variant, NULL);

    assert_string_equal("Users.Group.nogroup-group.", GET_CHAR(amxd_object_get_param_value(p_user_dm_obj, DM_GROUP_PARTICIPATION_NAME), NULL));
    assert_string_equal("Users.Role.untrusted-role.", GET_CHAR(amxd_object_get_param_value(p_user_dm_obj, DM_ROLE_PARTICIPATION_NAME), NULL));

    will_return(__wrap_sysconf_del_user, &expected_del_user);
    will_return(__wrap_sysconf_del_user, 0);
    amxd_object_delete(&p_user_dm_obj);

    _del_user_from_system("dm:instance-removed", p_event_variant, NULL);

    amxc_var_delete(&p_event_variant);

    teardown_default_dm();
}

void test_edit_user_password(UNUSED void** state) {
    userdata_t edited_user = test_user;
    userdata_t expected_user;
    user_dm_object_t edited_user_dm_object = test_user_dm_object;
    amxd_object_t* p_user_dm_obj = NULL;

    setup_default_dm(get_test_datamodel());

    edited_user_dm_object.m_password = NEW_PASSWORD;
    edited_user_dm_object.m_hashed_password = HASHED_PASSWORD;

    edited_user.password = NEW_PASSWORD;
    edited_user.hashed_password = NULL;

    expected_user = edited_user;
    expected_user.password = PASSWORD_SPECIAL_STR;
    expected_user.hashed_password = NEW_HASHED_PASSWORD;

    will_return(__wrap_crypt, NEW_PASSWORD);
    will_return(__wrap_crypt, NEW_HASHED_PASSWORD);

    add_user_to_dm(get_test_datamodel(), &edited_user_dm_object, &p_user_dm_obj);
    amxc_var_t* p_event_variant = convert_user_to_amxc_var_edit_string(&test_user_dm_object,
                                                                       "Password",
                                                                       PASSWORD,
                                                                       NEW_PASSWORD);

    will_return(__wrap_sysconf_edit_user, &expected_user);
    will_return(__wrap_sysconf_edit_user, 0);

    _edit_user_password_on_system("dm:object-changed", p_event_variant, NULL);

    dm_check_password(test_user.username, PASSWORD_SPECIAL_STR, NEW_HASHED_PASSWORD);

    amxc_var_delete(&p_event_variant);

    amxd_object_delete(&p_user_dm_obj);

    teardown_default_dm();
}


void test_edit_user_password_empty(UNUSED void** state) {
    userdata_t edited_user = test_user;
    userdata_t expected_user;
    user_dm_object_t edited_user_dm_object = test_user_dm_object;
    amxd_object_t* p_user_dm_obj = NULL;

    setup_default_dm(get_test_datamodel());

    edited_user_dm_object.m_password = PASSWORD_EMPTY_STR;
    edited_user_dm_object.m_hashed_password = PASSWORD_EMPTY_STR;

    edited_user.hashed_password = NULL;
    edited_user.password = PASSWORD_EMPTY_STR;

    expected_user = edited_user;
    expected_user.password = PASSWORD_SPECIAL_STR;
    expected_user.hashed_password = PASSWORD_EMPTY_STR;

    add_user_to_dm(get_test_datamodel(), &edited_user_dm_object, &p_user_dm_obj);
    amxc_var_t* p_event_variant = convert_user_to_amxc_var_edit_string(&test_user_dm_object,
                                                                       "Password",
                                                                       PASSWORD,
                                                                       PASSWORD_EMPTY_STR);

    will_return(__wrap_sysconf_edit_user, &expected_user);
    will_return(__wrap_sysconf_edit_user, 0);

    _edit_user_password_on_system("dm:object-changed", p_event_variant, NULL);

    dm_check_password(test_user.username, PASSWORD_SPECIAL_STR, PASSWORD_EMPTY_STR);

    amxc_var_delete(&p_event_variant);

    amxd_object_delete(&p_user_dm_obj);

    teardown_default_dm();
}

void test_edit_user_hashed_password(UNUSED void** state) {
    userdata_t edited_user = test_user;
    userdata_t expected_user;
    user_dm_object_t edited_user_dm_object = test_user_dm_object;
    amxd_object_t* p_user_dm_obj = NULL;

    setup_default_dm(get_test_datamodel());

    edited_user.hashed_password = NEW_HASHED_PASSWORD;

    edited_user_dm_object.m_password = PASSWORD_SPECIAL_STR;
    edited_user_dm_object.m_hashed_password = NEW_HASHED_PASSWORD;

    expected_user = edited_user;
    expected_user.password = PASSWORD_SPECIAL_STR;
    expected_user.hashed_password = NEW_HASHED_PASSWORD;

    add_user_to_dm(get_test_datamodel(), &edited_user_dm_object, &p_user_dm_obj);
    amxc_var_t* p_event_variant = convert_user_to_amxc_var_edit_string(&test_user_dm_object,
                                                                       "X_PRPL-COM_HashedPassword",
                                                                       HASHED_PASSWORD,
                                                                       NEW_HASHED_PASSWORD);
    will_return(__wrap_sysconf_edit_user, &expected_user);
    will_return(__wrap_sysconf_edit_user, 0);

    _edit_user_password_on_system("dm:object-changed", p_event_variant, NULL);

    dm_check_password(test_user.username, PASSWORD_SPECIAL_STR, NEW_HASHED_PASSWORD);

    amxc_var_delete(&p_event_variant);

    amxd_object_delete(&p_user_dm_obj);

    teardown_default_dm();
}

void test_edit_user_homedir(UNUSED void** state) {
    userdata_t edited_user = test_user;
    user_dm_object_t edited_user_dm_object = test_user_dm_object;
    amxd_object_t* p_user_dm_obj = NULL;

    setup_default_dm(get_test_datamodel());

    edited_user.hashed_password = "";
    edited_user.home_dir = "/root";
    edited_user_dm_object.m_homedir = "/root";

    add_user_to_dm(get_test_datamodel(), &edited_user_dm_object, &p_user_dm_obj);
    amxc_var_t* p_event_variant = convert_user_to_amxc_var_edit_string(&test_user_dm_object,
                                                                       "X_PRPL-COM_HomeDirectory",
                                                                       "/",
                                                                       "/root");
    will_return(__wrap_sysconf_edit_user, &edited_user);
    will_return(__wrap_sysconf_edit_user, 0);

    _edit_user_home_directory_on_system("dm:object-changed", p_event_variant, NULL);

    amxc_var_delete(&p_event_variant);

    amxd_object_delete(&p_user_dm_obj);

    teardown_default_dm();
}

void test_edit_user_shell(UNUSED void** state) {
    userdata_t edited_user = test_user;
    user_dm_object_t edited_user_dm_object = test_user_dm_object;
    shell_dm_object_t new_shell_dm_object = {
        .m_index = 0,
    };
    amxd_object_t* p_user_dm_obj = NULL;
    amxd_object_t* p_shell_dm_obj = NULL;

    new_shell_dm_object.m_alias = NEW_SHELL_ALIAS;
    new_shell_dm_object.m_name = NEW_SHELL_NAME;
    new_shell_dm_object.m_enable = true;

    setup_default_dm(get_test_datamodel());

    add_shell_to_dm(get_test_datamodel(), &new_shell_dm_object, &p_shell_dm_obj);

    edited_user.hashed_password = "";
    edited_user.shell_path = NEW_SHELL_NAME;

    edited_user_dm_object.m_shell = NEW_SHELL_PATH;

    add_user_to_dm(get_test_datamodel(), &edited_user_dm_object, &p_user_dm_obj);
    amxc_var_t* p_event_variant = convert_user_to_amxc_var_edit_string(&test_user_dm_object,
                                                                       "Shell",
                                                                       DEFAULT_SHELL_PATH,
                                                                       NEW_SHELL_PATH);

    will_return(__wrap_sysconf_edit_user, &edited_user);
    will_return(__wrap_sysconf_edit_user, 0);

    _edit_user_shell_on_system("dm:object-changed", p_event_variant, NULL);

    amxc_var_delete(&p_event_variant);

    amxd_object_delete(&p_user_dm_obj);
    amxd_object_delete(&p_shell_dm_obj);

    teardown_default_dm();
}

void test_edit_user_role_participation(UNUSED void** state) {
    userdata_t edited_user = test_user;
    user_dm_object_t edited_user_dm_object = test_user_dm_object;
    amxd_object_t* p_user_dm_obj = NULL;

    setup_default_dm(get_test_datamodel());

    edited_user.hashed_password = "";
    edited_user.role_paths = NEW_ROLE_PATH;

    edited_user_dm_object.m_role_participation = NEW_ROLE_PATH;

    add_user_to_dm(get_test_datamodel(), &edited_user_dm_object, &p_user_dm_obj);
    amxc_var_t* p_event_variant = convert_user_to_amxc_var_edit_string(&test_user_dm_object,
                                                                       "RoleParticipation",
                                                                       DEFAULT_ROLE_PATH,
                                                                       NEW_ROLE_PATH);
    will_return(__wrap_sysconf_edit_user, &edited_user);
    will_return(__wrap_sysconf_edit_user, 0);

    _edit_user_role_participation_on_system("dm:object-changed", p_event_variant, NULL);

    amxc_var_delete(&p_event_variant);

    amxd_object_delete(&p_user_dm_obj);

    teardown_default_dm();
}

void test_edit_user_group_participation(UNUSED void** state) {
    userdata_t edited_user = test_user;
    user_dm_object_t edited_user_dm_object = test_user_dm_object;
    amxd_object_t* p_user_dm_obj = NULL;

    groupdata_t group_data;
    group_dm_object_t group_dm_object;
    amxd_object_t* p_group_dm_obj = NULL;
    gid_t gid = 1234;

    setup_default_dm(get_test_datamodel());

    group_data.group_id = 1234;
    group_data.groupname = "newgroup";

    group_dm_object.m_alias = "new-group";
    group_dm_object.m_index = 38;
    group_dm_object.m_group_id = group_data.group_id;
    group_dm_object.m_group_name = group_data.groupname;
    group_dm_object.m_enable = true;
    group_dm_object.m_static_group = true;
    group_dm_object.m_role_participation = "";

    add_group_to_dm(get_test_datamodel(), &group_dm_object, &p_group_dm_obj);

    edited_user.hashed_password = "";
    edited_user.primary_group_id = 1234;
    edited_user.all_group_ids.size = 1;
    edited_user.all_group_ids.list = &gid;

    edited_user_dm_object.m_group_participation = DEFAULT_GROUP_PATH;

    add_user_to_dm(get_test_datamodel(), &edited_user_dm_object, &p_user_dm_obj);

    amxc_var_t* p_event_variant = convert_user_to_amxc_var_edit_string(&test_user_dm_object,
                                                                       "GroupParticipation",
                                                                       DEFAULT_GROUP_PATH,
                                                                       "Users.Group.new-group.");
    will_return(__wrap_sysconf_edit_user, &edited_user);
    will_return(__wrap_sysconf_edit_user, 0);

    _edit_user_group_participation_on_system("dm:object-changed", p_event_variant, NULL);

    amxc_var_delete(&p_event_variant);

    amxd_object_delete(&p_user_dm_obj);

    teardown_default_dm();
}

void test_edit_username(UNUSED void** state) {

    userdata_t edited_user = test_user;
    user_dm_object_t edited_user_dm_object = test_user_dm_object;
    amxd_object_t* p_user_dm_obj = NULL;

    setup_default_dm(get_test_datamodel());

    edited_user.hashed_password = "";
    edited_user.username = NEW_USERNAME;
    edited_user.previous_username = "test-user";

    add_user_to_dm(get_test_datamodel(), &edited_user_dm_object, &p_user_dm_obj);
    amxc_var_t* p_event_variant = convert_user_to_amxc_var_edit_string(&test_user_dm_object,
                                                                       "Username",
                                                                       "test-user",
                                                                       NEW_USERNAME);
    will_return(__wrap_sysconf_edit_user, &edited_user);
    will_return(__wrap_sysconf_edit_user, 0);

    _edit_user_name_on_system("dm:object-changed", p_event_variant, NULL);

    amxc_var_delete(&p_event_variant);

    amxd_object_delete(&p_user_dm_obj);

    teardown_default_dm();
}

void test_edit_empty_username(UNUSED void** state) {

    userdata_t edited_user = test_user;
    user_dm_object_t edited_user_dm_object = test_user_dm_object;
    amxd_object_t* p_user_dm_obj = NULL;

    setup_default_dm(get_test_datamodel());

    edited_user.hashed_password = "";
    edited_user.username = "testusr";
    edited_user.previous_username = "test-user";

    add_user_to_dm(get_test_datamodel(), &edited_user_dm_object, &p_user_dm_obj);
    amxc_var_t* p_event_variant = convert_user_to_amxc_var_edit_string(&test_user_dm_object,
                                                                       "Username",
                                                                       "test-user",
                                                                       "");
    will_return(__wrap_sysconf_edit_user, &edited_user);
    will_return(__wrap_sysconf_edit_user, 0);

    _edit_user_name_on_system("dm:object-changed", p_event_variant, NULL);

    amxc_var_delete(&p_event_variant);

    amxd_object_delete(&p_user_dm_obj);

    teardown_default_dm();
}

void test_edit_userid(UNUSED void** state) {

    userdata_t edited_user = test_user;
    user_dm_object_t edited_user_dm_object = test_user_dm_object;
    amxd_object_t* p_user_dm_obj = NULL;

    setup_default_dm(get_test_datamodel());

    edited_user.hashed_password = "";
    edited_user.user_id = 1234;

    add_user_to_dm(get_test_datamodel(), &edited_user_dm_object, &p_user_dm_obj);
    amxc_var_t* p_event_variant = convert_user_to_amxc_var_edit_string(&test_user_dm_object,
                                                                       "UserID",
                                                                       "666",
                                                                       "1234");
    will_return(__wrap_sysconf_edit_user, &edited_user);
    will_return(__wrap_sysconf_edit_user, 0);

    _edit_user_id_on_system("dm:object-changed", p_event_variant, NULL);

    amxc_var_delete(&p_event_variant);

    amxd_object_delete(&p_user_dm_obj);

    teardown_default_dm();
}

void test_disable_user(UNUSED void** state) {

    userdata_t edited_user = test_user;
    user_dm_object_t edited_user_dm_object = test_user_dm_object;
    amxd_object_t* p_user_dm_obj = NULL;

    setup_default_dm(get_test_datamodel());

    edited_user_dm_object.m_enable = true;

    edited_user.hashed_password = "";
    edited_user.enable = false;

    add_user_to_dm(get_test_datamodel(), &edited_user_dm_object, &p_user_dm_obj);
    amxc_var_t* p_event_variant = convert_user_to_amxc_var_edit_string(&test_user_dm_object,
                                                                       "Enable",
                                                                       "1",
                                                                       "0");
    will_return(__wrap_sysconf_edit_user, &edited_user);
    will_return(__wrap_sysconf_edit_user, 0);

    _edit_user_enable("dm:object-changed", p_event_variant, NULL);

    amxc_var_delete(&p_event_variant);

    amxd_object_delete(&p_user_dm_obj);

    teardown_default_dm();
}

void test_enable_user(UNUSED void** state) {

    userdata_t edited_user = test_user;
    user_dm_object_t edited_user_dm_object = test_user_dm_object;
    amxd_object_t* p_user_dm_obj = NULL;

    setup_default_dm(get_test_datamodel());

    edited_user_dm_object.m_enable = false;

    edited_user.hashed_password = "";
    edited_user.enable = true;

    add_user_to_dm(get_test_datamodel(), &edited_user_dm_object, &p_user_dm_obj);
    amxc_var_t* p_event_variant = convert_user_to_amxc_var_edit_string(&test_user_dm_object,
                                                                       "Enable",
                                                                       "0",
                                                                       "1");
    will_return(__wrap_sysconf_edit_user, &edited_user);
    will_return(__wrap_sysconf_edit_user, 0);

    _edit_user_enable("dm:object-changed", p_event_variant, NULL);

    amxc_var_delete(&p_event_variant);

    amxd_object_delete(&p_user_dm_obj);

    teardown_default_dm();

}

void test_add_user_with_bus_access_role(UNUSED void** state) {

    amxd_object_t* p_user_dm_obj = NULL;
    userdata_t expected_test_user = test_user;
    userdata_t expected_del_user = empty_user;
    user_dm_object_t user_dm_object = test_user_dm_object;
    amxc_string_t str;
    size_t len = 0;
    ssize_t read = 0;
    char* line = NULL;
    const char* file_path = "/usr/share/acl.d/test-user.json";
    bool is_directory_exist = false;
    bool username_found = false;
    int rv = 0;

    amxc_string_init(&str, 0);

    rv = amxp_dir_owned_make("/usr/share/acl.d/", 0777, 0, 0);
    assert_int_equal(rv, 0);
    is_directory_exist = fs_is_directory_exist("/usr/share/acl.d/");
    assert_true(is_directory_exist);

    setup_default_dm(get_test_datamodel());

    user_dm_object.m_role_participation = ACCESS_BUS_ROLE_PATH;
    user_dm_object.m_password = PASSWORD;
    user_dm_object.m_hashed_password = PASSWORD_EMPTY_STR;

    add_user_to_dm(get_test_datamodel(), &user_dm_object, &p_user_dm_obj);
    amxc_var_t* p_event_variant = convert_user_to_amxc_var(&user_dm_object);

    will_return(__wrap_crypt, PASSWORD);
    will_return(__wrap_crypt, HASHED_PASSWORD);

    expected_test_user.role_paths = ACCESS_BUS_ROLE_PATH;
    expected_test_user.password = PASSWORD_SPECIAL_STR;
    expected_test_user.hashed_password = HASHED_PASSWORD;

    will_return(__wrap_sysconf_add_user, &expected_test_user);
    will_return(__wrap_sysconf_add_user, 0);

    _sync_user_to_system("dm:instance-added", p_event_variant, NULL);

    assert_string_equal("Users.Role.5", GET_CHAR(amxd_object_get_param_value(p_user_dm_obj, DM_ROLE_PARTICIPATION_NAME), NULL));
    assert_int_equal(access(file_path, F_OK), 0);

    FILE* file = fopen(file_path, "r");
    assert_non_null(file);
    if(file != NULL) {
        while((read = getline(&line, &len, file)) != -1) {
            if(read > 0) {
                amxc_string_set(&str, line);
                if(amxc_string_search(&str, "test-user", 0) != -1) {
                    username_found = true;
                    break;
                }
                amxc_string_reset(&str);
            }
        }
    }
    assert_true(username_found);

    amxc_string_clean(&str);
    free(line);
    fclose(file);

    expected_del_user.username = "test-user";
    expected_del_user.user_id = test_user.user_id;

    will_return(__wrap_sysconf_del_user, &expected_del_user);
    will_return(__wrap_sysconf_del_user, 0);
    amxd_object_delete(&p_user_dm_obj);

    _del_user_from_system("dm:instance-removed", p_event_variant, NULL);

    assert_int_not_equal(access(file_path, F_OK), 0);

    amxc_var_delete(&p_event_variant);

    teardown_default_dm();
}

void test_edit_user_bus_access_role(UNUSED void** state) {
    userdata_t edited_user = test_user;
    user_dm_object_t edited_user_dm_object = test_user_dm_object;
    amxd_object_t* p_user_dm_obj = NULL;
    amxc_string_t str;
    size_t len = 0;
    ssize_t read = 0;
    char* line = NULL;
    const char* file_path = "/usr/share/acl.d/test-user.json";
    bool is_directory_exist = false;
    bool username_found = false;
    int rv = 0;

    amxc_string_init(&str, 0);

    rv = amxp_dir_owned_make("/usr/share/acl.d/", 0777, 0, 0);
    assert_int_equal(rv, 0);
    is_directory_exist = fs_is_directory_exist("/usr/share/acl.d/");
    assert_true(is_directory_exist);

    setup_default_dm(get_test_datamodel());

    edited_user.hashed_password = "";
    edited_user.role_paths = ACCESS_BUS_ROLE_PATH;

    edited_user_dm_object.m_role_participation = ACCESS_BUS_ROLE_PATH;

    add_user_to_dm(get_test_datamodel(), &edited_user_dm_object, &p_user_dm_obj);
    amxc_var_t* p_event_variant = convert_user_to_amxc_var_edit_string(&test_user_dm_object,
                                                                       "RoleParticipation",
                                                                       DEFAULT_ROLE_PATH,
                                                                       ACCESS_BUS_ROLE_PATH);
    will_return(__wrap_sysconf_edit_user, &edited_user);
    will_return(__wrap_sysconf_edit_user, 0);
    _edit_user_role_participation_on_system("dm:object-changed", p_event_variant, NULL);

    assert_string_equal("Users.Role.5", GET_CHAR(amxd_object_get_param_value(p_user_dm_obj, DM_ROLE_PARTICIPATION_NAME), NULL));
    assert_int_equal(access(file_path, F_OK), 0);

    FILE* file = fopen(file_path, "r");
    assert_non_null(file);
    if(file != NULL) {
        while((read = getline(&line, &len, file)) != -1) {
            if(read > 0) {
                amxc_string_set(&str, line);
                if(amxc_string_search(&str, "test-user", 0) != -1) {
                    username_found = true;
                    break;
                }
                amxc_string_reset(&str);
            }
        }
    }
    assert_true(username_found);

    amxc_string_clean(&str);
    free(line);
    fclose(file);

    amxc_var_delete(&p_event_variant);

    amxd_object_delete(&p_user_dm_obj);

    teardown_default_dm();
}

