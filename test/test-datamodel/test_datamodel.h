/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __TEST_DATAMODEL_H__
#define __TEST_DATAMODEL_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "mock_sysconf.h"
#include "mock_passwd_hash.h"

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>

#include <stdarg.h>
#include <setjmp.h>
#include <stdlib.h>
#include <cmocka.h>

void test_usermanagement_start(void** state);

void test_usermanagement_stop(void** state);

void test_entrypoint_unhandled_reason(void** state);

void test_add_user_with_empty_username(void** state);

void test_add_user_with_default_hashed_password(void** state);

void test_add_user_with_default_clear_password(void** state);

void test_add_user_with_group_and_role(void** state);

void test_add_user_with_empty_group_and_role(void** state);

void test_edit_user_password(void** state);

void test_edit_user_password_empty(void** state);

void test_edit_user_hashed_password(void** state);

void test_add_group(void** state);

void test_add_group_with_empty_role(void** state);

void test_edit_group_id(void** state);

void test_edit_group_name(void** state);

void test_edit_group_role(void** state);

void test_edit_user_homedir(void** state);

void test_edit_user_shell(void** state);

void test_edit_user_role_participation(void** state);

void test_edit_user_group_participation(void** state);

void test_edit_username(void** state);

void test_edit_empty_username(void** state);

void test_edit_userid(void** state);

void test_enable_user(void** state);

void test_add_user_with_bus_access_role(void** state);

void test_edit_user_bus_access_role(void** state);

void test_disable_user(void** state);

void test_check_delete_group(void** state);

void test_check_update_id(void** state);

int test_setup_dummy_backend();

int test_teardown_dummy_backend(void** state);

amxd_dm_t* get_test_datamodel(void);

amxo_parser_t* get_test_parser(void);

amxb_bus_ctx_t* get_test_bus_ctx(void);

#ifdef __cplusplus
}
#endif

#endif // __TEST_DATAMODEL_H__
