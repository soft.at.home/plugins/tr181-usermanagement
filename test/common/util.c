/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "util.h"
#include "common_test_data.h"

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>
#include <setjmp.h>
#include <cmocka.h>

void copy_file(const char* source, const char* dest) {
    int fd_source = open(source, O_RDONLY);
    struct stat source_stats;
    fstat(fd_source, &source_stats);
    int fd_dest = open(dest, O_WRONLY | O_TRUNC | O_CREAT, source_stats.st_mode);
    lseek(fd_source, 0, SEEK_SET);
    lseek(fd_dest, 0, SEEK_SET);

    copy_file_range(fd_source, NULL, fd_dest, NULL, source_stats.st_size, 0);
    sync();

    close(fd_source);
    close(fd_dest);
}

int backup_system_files(UNUSED void** state) {
    printf("*************************************\n");
    printf("*  This test should be run as root  *\n");
    printf("*************************************\n");
    fflush(stdout);
    // If these asserts fail, a backup still exists.
    // Set the backup back and remove the backup file
    assert_int_not_equal(access(GROUP_BACKUP_FILENAME, F_OK), 0);
    assert_int_not_equal(access(PASSWD_BACKUP_FILENAME, F_OK), 0);
    assert_int_not_equal(access(SHADOW_BACKUP_FILENAME, F_OK), 0);
    copy_file(PASSWD_FILENAME, PASSWD_BACKUP_FILENAME);
    copy_file(SHADOW_FILENAME, SHADOW_BACKUP_FILENAME);
    copy_file(GROUP_FILENAME, GROUP_BACKUP_FILENAME);
    sync();
    return 0;
}

int restore_system_files(UNUSED void** state) {
    remove(GROUP_FILENAME);
    remove(PASSWD_FILENAME);
    remove(SHADOW_FILENAME);
    copy_file(PASSWD_BACKUP_FILENAME, PASSWD_FILENAME);
    copy_file(SHADOW_BACKUP_FILENAME, SHADOW_FILENAME);
    copy_file(GROUP_BACKUP_FILENAME, GROUP_FILENAME);
    remove(GROUP_BACKUP_FILENAME);
    remove(PASSWD_BACKUP_FILENAME);
    remove(SHADOW_BACKUP_FILENAME);
    sync();
    return 0;
}

int test_setup_noop(UNUSED void** state) {
    return 0;
}

int test_teardown_noop(UNUSED void** state) {
    return 0;
}