# Changelog

All notable changes to this project must be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.23.15 - 2024-12-22(09:14:11 +0000)

### Other

- [SSH] Make getDebug working with non-root ssh user.

## Release v0.23.14 - 2024-10-25(16:50:03 +0000)

### Other

- - User default password must be locked and shell "bin/false"

## Release v0.23.13 - 2024-09-10(07:15:14 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v0.23.12 - 2024-08-30(11:46:17 +0000)

### Other

- - Users.User.{i}.X_PRPL-COM_HashedPassword must take precedence over Users.User.{i}.Password

## Release v0.23.11 - 2024-08-30(09:46:34 +0000)

### Other

- Add new user/group for certificates to allow mosquitto to load them when running as non-root

## Release v0.23.10 - 2024-08-02(13:54:57 +0000)

### Other

- - [tr181-usermanagement] Ensure that the original permissions and ownership are preserved for passwd, shadow, and group files.

## Release v0.23.9 - 2024-07-24(06:43:50 +0000)

### Other

- CLONE - [SSH] Root login doesn't require a password after reboot

## Release v0.23.8 - 2024-07-23(07:58:36 +0000)

### Fixes

- Better shutdown script

## Release v0.23.7 - 2024-07-22(13:11:12 +0000)

### Other

- [tr181-usermanagement] tr181-usermanagement crashes when trying to remove group

## Release v0.23.6 - 2024-07-05(15:45:54 +0000)

### Other

- [PCM] Backup/Restore does not restore new user

## Release v0.23.5 - 2024-06-25(14:00:08 +0000)

### Fixes

- Allow empty password when using hashed password storage in the data model (HOP-5540)

### Other

- Doc: Add documentation on 'Users.CreateNewUser()' api
- - Handle user enabling/disabling, account locking and empty password

## Release v0.23.4 - 2024-05-22(07:19:43 +0000)

### Fixes

- Remove wait for Users. in init script

## Release v0.23.3 - 2024-05-06(08:30:36 +0000)

### Fixes

- - Add possibility to create users without providing Username (and optimize transactions)

## Release v0.23.2 - 2024-04-25(11:08:47 +0000)

### Fixes

- - Prefer use of amxp dir tools to avoid dependency against libubox

## Release v0.23.1 - 2024-04-25(10:59:08 +0000)

### Fixes

- - Use '/var' homedir for chrony, dnsmasq and mosquitto

## Release v0.23.0 - 2024-04-24(15:36:13 +0000)

### New

- Issue: HOP-5914- Make Group[name/id] and RoleParticipation writable

## Release v0.22.0 - 2024-04-22(11:51:19 +0000)

### New

- - Make Username and RoleName writable

## Release v0.21.0 - 2024-04-22(11:39:07 +0000)

### New

- - Reorganize loggers into subsections

## Release v0.20.1 - 2024-04-15(17:16:10 +0000)

## Release v0.20.0 - 2024-04-12(07:21:29 +0000)

### New

- - Handle home dir creation and permission

## Release v0.19.2 - 2024-04-10(07:12:50 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.19.1 - 2024-04-04(10:33:17 +0000)

### Fixes

- - Synchronize UID and GID to align with openwrt system

## Release v0.19.0 - 2024-03-23(13:08:58 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.18.2 - 2024-03-21(09:06:56 +0000)

### Fixes

- After reboot all hosts are disconnected (AKA amb timeouts)

## Release v0.18.1 - 2024-03-14(18:53:37 +0000)

### Fixes

- - Fix restoration of user data model unsaved fields after an upgrade

## Release v0.18.0 - 2024-03-04(09:01:31 +0000)

### New

- - Allow users to have the same UID

## Release v0.17.3 - 2024-02-28(13:48:19 +0000)

### Fixes

- - When computing hashed password, use transaction api since it's mandatory for pcm mgr

## Release v0.17.2 - 2024-02-26(09:05:06 +0000)

### Fixes

- - Remove group and role associations from user instances when deleting them

## Release v0.17.1 - 2024-02-21(11:34:09 +0000)

### Fixes

- - [ACL] Rework admin acl logic / admin should be able to create non static users

## Release v0.17.0 - 2024-02-20(10:27:31 +0000)

### New

- - Create ssh_wan/lan groups and users in default odl

## Release v0.16.1 - 2024-02-19(07:46:47 +0000)

### Other

- [tr181-Users][SSH] Add internal API to change password for ssh user SSH on prpl the Router

## Release v0.16.0 - 2024-02-09(14:24:47 +0000)

### New

- - Split ODL defaults for users (system and logical)

## Release v0.15.7 - 2024-02-03(07:51:39 +0000)

### Fixes

- [TR181-Users] RemoteAccessCapable is deprecated

## Release v0.15.6 - 2024-01-30(13:13:55 +0000)

### Fixes

- - [tr181-usermanagement] Allow empty password when password isn't stored hashed in the data model

## Release v0.15.5 - 2024-01-18(13:46:34 +0000)

### Changes

- Issue HOP-5447 - The users defined in the ODL take priority over those in the system

## Release v0.15.4 - 2024-01-17(08:35:46 +0000)

### Other

- - [ACL] Admin can read Device.Users.Group.

## Release v0.15.3 - 2024-01-11(13:49:39 +0000)

### Fixes

- - Fix missing hashed password prefix in CreateNewUser api

## Release v0.15.2 - 2024-01-11(12:12:23 +0000)

### Other

- [CI] Disable uncrustify

## Release v0.15.1 - 2024-01-11(09:51:12 +0000)

### Fixes

- - X_PRPL-COM_HomeDirectory must be public in DM

## Release v0.15.0 - 2024-01-08(17:18:21 +0000)

### New

- - Store user's password in data model in hashed form (SHA-512)

## Release v0.14.4 - 2024-01-04(12:39:19 +0000)

### Changes

- - Review UIDs/GIDs in usermanagement defaults and change starting UID

## Release v0.14.3 - 2023-11-30(13:47:25 +0000)

### Other

- - [prpl][tr181-usermanagement] CreateNewUser always fails with error 21

## Release v0.14.2 - 2023-11-29(08:12:25 +0000)

### Fixes

- [SAFRAN_PRPL] Administration password is not upgrade persistent

## Release v0.14.1 - 2023-11-10(13:58:11 +0000)

### Other

- - Update default roles in mainline configuration

## Release v0.14.0 - 2023-10-17(13:50:05 +0000)

### New

- - [ACL] Admin cannot access to Device.Users. :missing config in webui.json

## Release v0.13.1 - 2023-10-13(13:28:07 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v0.13.0 - 2023-10-11(06:44:48 +0000)

### New

- [amxrt][no-root-user][capability drop] All amx plugins must be adapted to run as non-root and lmited capabilities

## Release v0.12.0 - 2023-09-22(12:23:15 +0000)

### New

- [UserLogs][User Password management] Add User Logs for Password manangement

## Release v0.11.0 - 2023-09-21(11:24:52 +0000)

### New

- [amxrt][no-root-user][capability drop] Netdev plugin must be adapted to run as non-root and lmited capabilities

## Release v0.10.1 - 2023-09-19(08:28:37 +0000)

### Fixes

- [amx][tr181-usermanagement] Add functionality to define safe password rules

## Release v0.10.0 - 2023-09-12(11:02:21 +0000)

### New

- [amx][tr181-usermanagement] Add functionality to define safe password rules

## Release v0.9.0 - 2023-09-07(15:25:11 +0000)

### New

- [amxrt][no-root-user][capability drop] [tr181-dhcpv4client] tr181-dhcpv4client plugin must be adapted to run as non-root and lmited capabilities

## Release v0.8.2 - 2023-08-28(14:43:53 +0000)

### Other

- [tr181-usermanagement] Crash due to nullpointer at startup

## Release v0.8.1 - 2023-08-22(06:54:29 +0000)

### Fixes

- - [prpl][user-management] Users role paths are not tr181 paths

## Release v0.8.0 - 2023-08-02(07:05:41 +0000)

### New

- - [WebUI][UserManagement] Enable a proper admin login in default config

## Release v0.7.9 - 2023-07-13(07:27:55 +0000)

### Fixes

- [USP] GSDM should return whether commands are (a)sync

## Release v0.7.8 - 2023-07-03(13:25:54 +0000)

### Fixes

- Init script has no shutdown function

## Release v0.7.7 - 2023-06-02(08:56:19 +0000)

### Fixes

- Multiple possible points can cause segfault

## Release v0.7.6 - 2023-05-23(09:06:09 +0000)

### Changes

- - [HTTPManager][Login][amx-fcgi] Create a session

## Release v0.7.5 - 2023-03-23(09:29:22 +0000)

### Fixes

- [tr181-usermanagement] Datamodel changes are not reboot persistent

## Release v0.7.4 - 2023-03-09(09:19:32 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v0.7.3 - 2023-02-13(15:38:28 +0000)

## Release v0.7.2 - 2023-02-06(19:43:50 +0000)

### Fixes

- [UserManagement] Excessive writes to passwd,group,shadow on start

## Release v0.7.1 - 2023-02-03(13:46:26 +0000)

### Fixes

- [UserManagement] Excessive writes to passwd,group,shadow on start

## Release v0.7.0 - 2023-01-09(13:49:58 +0000)

### New

- [tr181 Usermanagement] Add %out parameter to the CheckCredentialsDiagnostics function

## Release v0.6.14 - 2022-12-20(18:44:56 +0000)

### Fixes

- Do not remove files before overwriting them

## Release v0.6.13 - 2022-12-09(09:29:53 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v0.6.12 - 2022-11-17(16:34:05 +0000)

### Fixes

- gcc 11.2.0 linker cannot find -lsahtrace

## Release v0.6.11 - 2022-11-11(10:16:35 +0000)

### Fixes

- [TR181 Usermanagement] Add dependency on lua-amx

## Release v0.6.10 - 2022-10-06(08:04:57 +0000)

### Other

- set import-dbg to false

## Release v0.6.9 - 2022-08-15(06:56:03 +0000)

### Other

- [tr181-usermanagement] Fix behavior for empty passwords

## Release v0.6.8 - 2022-07-05(07:48:46 +0000)

## Release v0.6.7 - 2022-06-16(06:49:35 +0000)

### Fixes

- Plugins not starting at boot

## Release v0.6.6 - 2022-05-30(08:52:58 +0000)

### Other

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.6.5 - 2022-05-27(08:08:01 +0000)

### Other

- [prpl][TR181-Usermanagement] Add extra function to check Username/Password

## Release v0.6.4 - 2022-05-23(19:46:12 +0000)

### Other

- [prpl][TR181-Usermanagement] Add extra function to check Username/Password.

## Release v0.6.3 - 2022-04-19(07:50:10 +0000)

### Fixes

- Design add/delete handlers

## Release v0.6.2 - 2022-03-24(10:21:16 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v0.6.1 - 2022-02-25(10:56:06 +0000)

### Other

- Enable core dumps by default

## Release v0.6.0 - 2022-01-10(15:17:16 +0000)

### New

- Add default file matching hwg config

## Release v0.5.0 - 2022-01-06(22:00:10 +0000)

### New

- Add invalid passwords

## Release v0.4.0 - 2021-12-27(08:58:43 +0000)

### New

- [TR181 Usermanagement] Add private home directory parameter to Users.User

## Release v0.3.1 - 2021-12-22(20:58:13 +0000)

### Fixes

- [TR181 Usermanagement] Creating user on GL-B1300 wipes /etc/shadow

## Release v0.3.0 - 2021-12-21(21:39:03 +0000)

### New

- [TR181 Usermanagement] Add build option for SHA512

### Changes

- Add event and action handler unit tests

## Release v0.2.4 - 2021-11-24(14:35:23 +0000)

### Fixes

- [TR181 Usermanagement] Don't fail on nonexistent groups

### Changes

- Add first unit tests

## Release v0.2.3 - 2021-11-23(08:17:56 +0000)

### Other

- [ACL] Usermanagement plugin must provide default Role configuration

## Release v0.2.2 - 2021-11-17(10:12:03 +0000)

### Changes

- [TR181-usermanagement] Always map users to Linux

## Release v0.2.1 - 2021-11-08(12:08:16 +0000)

### Fixes

- [TR181 Usermanagement] map users to linux users

## Release v0.2.0 - 2021-11-08(09:36:59 +0000)

### New

- map users to linux users

## Release v0.1.0 - 2021-09-30(08:13:21 +0000)

### New

- Initial release of the datamodel

