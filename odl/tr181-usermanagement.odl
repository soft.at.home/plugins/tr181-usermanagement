#include "mod_sahtrace.odl";
#include "mod_apparmor.odl";
#include "global_amxb_timeouts.odl";

%config {
    // Application specific settings
    // persistent storage location
    rw_data_path = "${prefix}/etc/config";
    storage-path = "${rw_data_path}/${name}";

    %global prefix_ = "X_PRPL-COM_";

    import-dbg = false;

    storage-type="odl";

    // Handle data model startup events before start event
    dm-events-before-start = true;

    ubus = {
        // register data model to ubus on start event
        register-on-start-event = true
    };

    pcb = {
        register-on-start-event = true
    };

    privileges = {
        keep-all = true
    };

    odl = {
        dm-save = true,
        dm-save-on-changed = true,
        dm-save-delay = 1000,
        dm-load = true,
        dm-defaults = "${name}_defaults.d/",
        load-dm-events = true,
        directory = "${storage-path}/odl"
    };

    // main files
    definition_file = "${name}_definition.odl";

    sahtrace = {
        type = "syslog",
        level = "${default_log_level}"
    };

    trace-zones = {
        "main" = "${default_trace_zone_level}",
        "dm" = "${default_trace_zone_level}",
        "action" = "${default_trace_zone_level}",
        "rpc" = "${default_trace_zone_level}",
        "sysconf" = "${default_trace_zone_level}",
        "passwd" = "${default_trace_zone_level}",
        "shadow" = "${default_trace_zone_level}",
        "group" = "${default_trace_zone_level}",
        "db" = "${default_trace_zone_level}",
        "fs" = "${default_trace_zone_level}",
        "misc" = "${default_trace_zone_level}",
        "access_role" = "${default_trace_zone_level}"
    };

    admin_pwd = "$(DEFAULT_ADMIN_PASSWORD)";
    starting_uid = 1000;

    pcm_svc_config = {
        "Objects" = "Users"
    };
}

import "mod-dmext.so";
import "${name}.so" as "${name}";

include "${definition_file}";
#include "mod-users-passwordvalidation.odl";

%define {
    entry-point tr181-usermanagement.usermanagement_main;
}

#include "mod_pcm_svc.odl";
