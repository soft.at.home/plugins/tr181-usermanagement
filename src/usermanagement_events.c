/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "dm_usermanagement.h"

#include "usermanagement.h"
#include "usermanagement_util.h"
#include "passwd_hash.h"
#include "sysconf.h"
#include "file_system.h"
#include "access_role.h"

#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define ME "dm"

#define INSTANCE_ADDED_EVENT_NAME "dm:instance-added"
#define INSTANCE_REMOVED_EVENT_NAME "dm:instance-removed"
#define OBJECT_CHANGED_EVENT_NAME "dm:object-changed"

#define USER_PATH_PREFIX "Users.User."
#define USER_PATH_PREFIX_LEN 11

#define TRACE_I_EVENT(f_name, e_name) \
    SAH_TRACEZ_INFO(ME, "Called event handler '%s' on event '%s'", f_name, e_name)
#define TRACE_W_WKOE(expected, got) \
    SAH_TRACEZ_WARNING(ME, \
                       "Called event handler for wrong kind of event. Expected '%s', got '%s'", \
                       expected, \
                       got);


static amxd_object_t* dm_get_user_instance(const char* alias) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* users = NULL;
    amxd_object_t* user = NULL;

    when_null(alias, exit);
    users = amxd_dm_findf(usermanagement_get_dm(), "Users.User.");
    when_null_trace(users, exit, INFO, "Users object is NULL");
    user = amxd_object_findf(users, "[" "Alias" " == '%s']", alias);
    when_null_trace(user, exit, INFO, "Failed to find user with Alias = %s", alias);
exit:
    SAH_TRACEZ_OUT(ME);
    return user;
}

static amxd_object_t* dm_get_group_instance(const char* groupname) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* groups = NULL;
    amxd_object_t* group = NULL;

    when_null(groupname, exit);
    groups = amxd_dm_findf(usermanagement_get_dm(), "Users.Group.");
    when_null_trace(groups, exit, INFO, "Group object is NULL");
    group = amxd_object_findf(groups, "[" "Groupname" " == '%s']", groupname);
    when_null_trace(group, exit, INFO, "Failed to find user with Groupname = %s", groupname);
exit:
    SAH_TRACEZ_OUT(ME);
    return group;
}

static bool check_and_reset_internal_dm_update(const amxd_object_t* p_object, const amxc_var_t* edit_event_data) {
    SAH_TRACEZ_IN(ME);
    // The purpose of this function is to prevent an infinite loopback of events.
    // The plugin modifies itself as it computes hashed passwords based on the clear password given.
    // Therefore, when the hashed password is computed and data model updated, we need to detect that.

    amxd_param_t* internal_update_param = NULL;
    amxc_var_t internal_update;
    amxc_var_init(&internal_update);
    bool is_internal = GETP_BOOL(edit_event_data, "parameters.IsInternalUpdateTrigger.to");

    when_false(is_internal, exit);
    internal_update_param = amxd_object_get_param_def(p_object, "IsInternalUpdateTrigger");
    when_null_trace(internal_update_param, exit, ERROR, "User IsInternalUpdateTrigger is NULL");
    amxc_var_set(bool, &internal_update, false);
    amxd_param_set_value(internal_update_param, &internal_update);

exit:
    amxc_var_clean(&internal_update);
    SAH_TRACEZ_OUT(ME);
    return is_internal;
}

static void mark_as_internal_dm_update(amxd_trans_t* trans) {
    SAH_TRACEZ_IN(ME);
    amxd_trans_set_bool(trans, "IsInternalUpdateTrigger", true);
    SAH_TRACEZ_OUT(ME);
}

static amxd_status_t dm_transaction_open(amxd_trans_t* const trans, amxd_object_t* p_object) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_invalid_value;

    when_null(trans, exit);
    when_null(p_object, exit);

    status = amxd_trans_init(trans);
    when_failed(status, exit);
    status = amxd_trans_set_attr(trans, amxd_tattr_change_priv, true);
    when_failed(status, exit);
    status = amxd_trans_select_object(trans, p_object);
    when_failed(status, exit);

    status = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static void dm_transaction_close(amxd_trans_t* const trans) {
    SAH_TRACEZ_IN(ME);
    when_null(trans, exit);
    amxd_trans_clean(trans);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static amxd_status_t dm_transaction_apply(amxd_trans_t* trans) {

    amxd_status_t status = amxd_status_invalid_value;
    when_null(trans, exit);
    status = amxd_trans_apply(trans, usermanagement_get_dm());
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static void dm_overload_user_name(amxd_trans_t* trans, const userdata_t* userdata) {
    SAH_TRACEZ_IN(ME);

    when_null(trans, exit);
    when_null(userdata, exit);

    amxd_trans_set_cstring_t(trans, DM_USERNAME, userdata->username);
    mark_as_internal_dm_update(trans);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static bool dm_overload_group_empty_fields(amxd_trans_t* trans, amxd_object_t* p_object, UNUSED const char* name) {
    SAH_TRACEZ_IN(ME);
    bool update_needed = false;

    when_null(trans, exit);
    when_null(p_object, exit);

    if(STRING_EMPTY(GET_CHAR(amxd_object_get_param_value(p_object, DM_GROUP_PARTICIPATION_NAME), NULL))) {
        SAH_TRACEZ_INFO(ME, "Overload group with %s for %s.", DM_USER_GROUP_PARTICIPATION_DEFAULT, name);
        amxd_trans_set_cstring_t(trans, DM_GROUP_PARTICIPATION_NAME, DM_USER_GROUP_PARTICIPATION_DEFAULT);
        mark_as_internal_dm_update(trans);
        update_needed = true;
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return update_needed;
}

static bool dm_overload_role_empty_fields(amxd_trans_t* trans, amxd_object_t* p_object, UNUSED const char* name) {
    SAH_TRACEZ_IN(ME);
    bool update_needed = false;
    when_null(trans, exit);
    when_null(p_object, exit);

    if(STRING_EMPTY(GET_CHAR(amxd_object_get_param_value(p_object, DM_ROLE_PARTICIPATION_NAME), NULL))) {
        SAH_TRACEZ_INFO(ME, "Overload role with %s for %s.", DM_USER_ROLE_PARTICIPATION_DEFAULT, name);
        amxd_trans_set_cstring_t(trans, DM_ROLE_PARTICIPATION_NAME, DM_USER_ROLE_PARTICIPATION_DEFAULT);
        mark_as_internal_dm_update(trans);
        update_needed = true;
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return update_needed;

}

static bool dm_overload_group_fields_if_needed(amxd_trans_t* trans, amxd_object_t* p_object, const groupdata_t* goupdata) {
    SAH_TRACEZ_IN(ME);
    bool update_needed = false;

    when_null(trans, exit);
    when_null(p_object, exit);
    when_null(goupdata, exit);

    update_needed = dm_overload_role_empty_fields(trans, p_object, goupdata->groupname);
exit:
    SAH_TRACEZ_OUT(ME);
    return update_needed;
}

static bool dm_overload_shell_empty_fields(amxd_trans_t* trans, amxd_object_t* p_object, UNUSED const char* username) {
    SAH_TRACEZ_IN(ME);
    bool update_needed = false;

    when_null(trans, exit);
    when_null(p_object, exit);

    if(STRING_EMPTY(GET_CHAR(amxd_object_get_param_value(p_object, DM_SHELL_NAME), NULL))) {
        SAH_TRACEZ_INFO(ME, "Overload shell with %s for %s.", DM_USER_SUPPORTED_SHELL_DEFAULT, username);
        amxd_trans_set_cstring_t(trans, DM_SHELL_NAME, DM_USER_SUPPORTED_SHELL_DEFAULT);
        mark_as_internal_dm_update(trans);
        update_needed = true;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return update_needed;
}

static bool dm_overload_user_fields_if_needed(amxd_trans_t* trans, amxd_object_t* p_user_object, const userdata_t* userdata) {
    SAH_TRACEZ_IN(ME);
    bool update_needed = false;

    update_needed |= dm_overload_shell_empty_fields(trans, p_user_object, userdata->username);
    update_needed |= dm_overload_group_empty_fields(trans, p_user_object, userdata->username);
    update_needed |= dm_overload_role_empty_fields(trans, p_user_object, userdata->username);

    SAH_TRACEZ_OUT(ME);
    return update_needed;
}

static int32_t dm_update_user_password(amxd_trans_t* trans, UNUSED const char* username,
                                       const char* input_clear_password, const char* input_hashed_password) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;

    when_null(trans, exit);
    SAH_TRACEZ_INFO(ME, "Update stored hashed password for user %s", username);
    amxd_trans_set_cstring_t(trans, DM_PASSWD_NAME, input_clear_password);
    amxd_trans_set_cstring_t(trans, get_hashed_password_path(), input_hashed_password);
    mark_as_internal_dm_update(trans);
    retval = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static int32_t fill_userdata_password(userdata_t* p_to_fill, const amxc_var_t* const user_parameters) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    const char* input_password = GET_CHAR(user_parameters, DM_PASSWD_NAME);
    const char* input_hashed_password = GET_CHAR(user_parameters, get_hashed_password_path());

    when_str_empty(p_to_fill->username, exit);

    // initialized the password
    p_to_fill->password = PASSWORD_SPECIAL_STR;

    if(!STRING_EMPTY(input_hashed_password)) {
        // password is set using Users.User.{i}.X_PRPL-COM_HashedPassword
        // no need to update the dm here
        p_to_fill->hashed_password = input_hashed_password;
    } else if(STRING_EMPTY(input_password) || (strcmp(input_password, PASSWORD_SPECIAL_STR) != 0)) {
        // password set using Users.User.{i}.Password
        p_to_fill->hashed_password = generate_password_hash(p_to_fill->username, input_password);
    } else {
        p_to_fill->hashed_password = input_hashed_password;
    }

    retval = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static int32_t fill_userdata_homedir(userdata_t* p_to_fill, const amxc_var_t* const p_home_dir) {
    SAH_TRACEZ_IN(ME);
    int retval = -1;

    when_null_trace(p_to_fill, exit, ERROR, "No userdata provided");
    when_null_trace(p_home_dir, exit, ERROR, "No path to home directory provided");

    p_to_fill->home_dir = GET_CHAR(p_home_dir, NULL);
    retval = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static int32_t fill_userdata_role_paths(userdata_t* p_to_fill, const amxc_var_t* const p_role_paths) {
    SAH_TRACEZ_IN(ME);
    int retval = -1;

    when_null_trace(p_to_fill, exit, ERROR, "No userdata provided");
    when_null_trace(p_role_paths, exit, INFO, "No path to role paths provided");

    p_to_fill->role_paths = GET_CHAR(p_role_paths, NULL);
    retval = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static int32_t fill_userdata_name_if_empty(userdata_t* userdata, const char* alias) {
    int32_t retval = -1;

    when_null(userdata, exit);
    when_null(alias, exit);

    if(STRING_EMPTY(userdata->username)) {
        userdata->username = alias;
        userdata->previous_username = "";
    }

    retval = 0;
exit:
    return retval;
}

static int32_t fill_userdata_id(userdata_t* const p_to_fill,
                                const amxc_var_t* const p_id_params) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;

    when_null_trace(p_to_fill, exit, ERROR, "No userdata provided");
    when_null_trace(p_id_params, exit, ERROR, "No user ID information provided");

    p_to_fill->user_id = GET_UINT32(p_id_params, DM_USERID);
    p_to_fill->username = GET_CHAR(p_id_params, DM_USERNAME);
    p_to_fill->alias = GET_CHAR(p_id_params, DM_ALIAS);

    when_failed_trace(fill_userdata_name_if_empty(p_to_fill, p_to_fill->alias), exit, ERROR, "Failed to edit User on system");

    retval = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static int32_t fill_userdata_enable(userdata_t* const p_to_fill,
                                    const amxc_var_t* const p_id_params) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;

    when_null_trace(p_to_fill, exit, ERROR, "No userdata provided");
    when_null_trace(p_id_params, exit, ERROR, "No user ID information provided");
    p_to_fill->enable = GET_BOOL(p_id_params, DM_ENABLE_NAME);
    retval = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static int32_t fill_userdata(userdata_t* const to_fill,
                             const amxc_var_t* const data) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    const amxc_var_t* const user_parameters = amxc_var_get_path(data,
                                                                "parameters",
                                                                AMXC_VAR_FLAG_DEFAULT);
    const char* user_path = GET_CHAR(data, "object");
    char* buf = NULL;
    amxd_object_t* root_object = NULL;
    amxd_object_t* user_object = NULL;
    const amxc_var_t* p_shell_path = NULL;
    const amxc_var_t* p_home_dir = NULL;
    const amxc_var_t* user_group_csvlist = NULL;
    const amxc_var_t* user_role_csvlist = NULL;
    const char* name = GET_CHAR(data, "name");

    when_null_trace(to_fill, exit, ERROR, "No userdata to fill provided");
    when_null_trace(user_path, exit, ERROR, "No user path found");
    if(strcmp(user_path, USER_PATH_PREFIX) == 0) {
        buf = (char*) calloc(USER_PATH_PREFIX_LEN + strlen(name) + 1, sizeof(char));
        when_null_oom(buf, exit, "user_path");
        sprintf(buf, "Users.User.%s", name);
        user_path = buf;
    }

    root_object = amxd_dm_get_root(usermanagement_get_dm());
    user_object = amxd_object_findf(root_object, "%s", user_path);

    when_failed_trace(fill_userdata_id(to_fill, user_parameters), exit, ERROR, "Failed to fill ID of User");
    when_failed_trace(fill_userdata_password(to_fill, user_parameters), exit, ERROR, "Failed to fill password of User");
    when_failed_trace(fill_userdata_enable(to_fill, user_parameters), exit, ERROR, "Failed to fill state (enable) of User");

    p_shell_path = amxc_var_get_key(user_parameters, "Shell", AMXC_VAR_FLAG_DEFAULT);
    when_failed_trace(fill_userdata_shell(to_fill, p_shell_path), exit, ERROR, "Failed to fill Shell of User");

    p_home_dir = amxd_object_get_param_value(user_object, get_home_directory_path());
    when_failed_trace(fill_userdata_homedir(to_fill, p_home_dir), exit, ERROR, "Failed to fill home directory of User");

    user_group_csvlist = amxc_var_get_key(user_parameters, "GroupParticipation", AMXC_VAR_FLAG_DEFAULT);
    retval = fill_userdata_groups(to_fill, user_group_csvlist);
    when_failed_trace(retval, exit, ERROR, "Failed to fill Groups of User");

    user_role_csvlist = amxc_var_get_key(user_parameters, "RoleParticipation", AMXC_VAR_FLAG_DEFAULT);
    retval = fill_userdata_role_paths(to_fill, user_role_csvlist);
    when_failed_trace(retval, exit, ERROR, "Failed to fill Roles of User");

exit:
    free(buf);
    buf = NULL;
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static int32_t fill_userdata_password_on_edit(userdata_t* p_to_fill, const amxc_var_t* edit_event_data) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    const char* input_password = NULL;
    const char* input_hashed_password = NULL;
    amxc_string_t hashed_password_path;

    amxc_string_init(&hashed_password_path, 0);

    when_null_trace(p_to_fill, exit, ERROR, "No userdata to fill provided");
    when_null_trace(edit_event_data, exit, ERROR, "No value provided");

    amxc_string_setf(&hashed_password_path, "parameters.%s.to", get_hashed_password_path());

    input_password = GETP_CHAR(edit_event_data, "parameters.Password.to");
    input_hashed_password = GETP_CHAR(edit_event_data, amxc_string_get(&hashed_password_path, 0));

    p_to_fill->password = PASSWORD_SPECIAL_STR;

    if(!STRING_EMPTY(input_hashed_password)) {
        p_to_fill->hashed_password = input_hashed_password;
    } else if(STRING_EMPTY(input_password) || (strcmp(input_password, PASSWORD_SPECIAL_STR) != 0)) {
        p_to_fill->hashed_password = generate_password_hash(p_to_fill->username, input_password);

    } else {
        p_to_fill->hashed_password = input_hashed_password;
    }

    retval = 0;

exit:
    amxc_string_clean(&hashed_password_path);
    SAH_TRACEZ_OUT(ME);
    return retval;
}

void _sync_user_to_system(const char* const event_name,
                          const amxc_var_t* event_data,
                          UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    userdata_t userdata = {0};
    amxd_object_t* p_user_object = NULL;
    amxd_trans_t user_tansaction;
    int rv = 0;
    TRACE_I_EVENT("sync_user_to_system", event_name);

    amxd_trans_init(&user_tansaction);
    init_userdata(&userdata);

    if(strcmp(event_name, INSTANCE_ADDED_EVENT_NAME) != 0) {
        TRACE_W_WKOE(INSTANCE_ADDED_EVENT_NAME, event_name);
        goto exit;
    }

    if(fill_userdata(&userdata, event_data) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to fill User data. User will not be synced.");
        goto exit;
    }

    rv = sysconf_add_user(&userdata);
    if(rv == ENTRY_EXISTS) {
        SAH_TRACEZ_INFO(ME, "User %s already exists in the system.", userdata.username);
    } else if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to add %s user to system.", userdata.username);
    }

    if(fs_is_directory_exist(userdata.home_dir) == false) {
        when_failed(fs_create_owned_directory(userdata.home_dir, &userdata), exit);
    } else {
        SAH_TRACEZ_INFO(ME, "home dir [%s] already exist for %s", userdata.home_dir, userdata.username);
    }

    p_user_object = dm_get_user_instance(userdata.alias);
    when_null_trace(p_user_object, exit, INFO, "Failed to find user with Username = %s", userdata.alias);
    when_failed_trace(dm_transaction_open(&user_tansaction, p_user_object), exit, ERROR, "Failed to initialize transaction");

    SAH_TRACEZ_INFO(ME, "Update password in data model for %s.", userdata.username);
    if(dm_update_user_password(&user_tansaction,
                               userdata.username,
                               userdata.password,
                               userdata.hashed_password) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to update password in data model.");
    }

    if(STRING_EMPTY(userdata.previous_username)) {
        // No username provided, it has been overloaded with alias, data model must be updated
        dm_overload_user_name(&user_tansaction, &userdata);
    }

    dm_overload_user_fields_if_needed(&user_tansaction, p_user_object, &userdata);
    when_failed_trace(dm_transaction_apply(&user_tansaction), exit, ERROR, "Update transaction failed for user %s", userdata.username);
    access_role_manage_user_bus_role(&userdata);

exit:
    dm_transaction_close(&user_tansaction);
    clean_userdata(&userdata);
    SAH_TRACEZ_OUT(ME);
    return;
}

void _sync_group_to_system(const char* const event_name,
                           const amxc_var_t* event_data,
                           UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    groupdata_t group_data;
    amxd_object_t* p_group_object = NULL;
    amxd_trans_t group_transaction;
    int rv = 0;

    TRACE_I_EVENT("sync_group_to_system", event_name);

    amxd_trans_init(&group_transaction);
    init_groupdata(&group_data);

    if(strcmp(event_name, INSTANCE_ADDED_EVENT_NAME) != 0) {
        TRACE_W_WKOE(INSTANCE_ADDED_EVENT_NAME, event_name);
        goto exit;
    }

    fill_groupdata(&group_data, event_data);

    rv = sysconf_add_group(&group_data);
    if(rv == ENTRY_EXISTS) {
        SAH_TRACEZ_INFO(ME, "Group already exists in the system");
    } else if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to add Group to system");
    }

    p_group_object = dm_get_group_instance(group_data.groupname);
    when_null(p_group_object, exit);

    when_failed_trace(dm_transaction_open(&group_transaction, p_group_object), exit, ERROR, "Failed to initialize transaction");
    when_false(dm_overload_group_fields_if_needed(&group_transaction, p_group_object, &group_data), exit);
    when_failed_trace(dm_transaction_apply(&group_transaction), exit, ERROR, "Update transaction failed for group %s", group_data.groupname);

exit:
    dm_transaction_close(&group_transaction);
    clean_groupdata(&group_data);
    SAH_TRACEZ_OUT(ME);
}

static inline bool userdata_is_empty(const userdata_t user_data) {
    SAH_TRACEZ_IN(ME);
    bool rv = user_data.username == NULL &&
        user_data.hashed_password == NULL &&
        user_data.home_dir == NULL &&
        user_data.shell_path == NULL &&
        user_data.role_paths == NULL &&
        user_data.all_group_ids.list == NULL;
    SAH_TRACEZ_OUT(ME);
    return rv;
}

void _edit_user_role_participation_on_system(const char* const event_name,
                                             const amxc_var_t* event_data,
                                             UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    userdata_t userdata = {0};
    amxd_object_t* p_user_object = NULL;
    amxd_trans_t user_tansaction;

    TRACE_I_EVENT("edit_user_role_participation", event_name);

    amxd_trans_init(&user_tansaction);
    init_userdata(&userdata);

    if(strcmp(event_name, OBJECT_CHANGED_EVENT_NAME) != 0) {
        TRACE_W_WKOE(OBJECT_CHANGED_EVENT_NAME, event_name);
        goto exit;
    }

    p_user_object = amxd_dm_signal_get_object(usermanagement_get_dm(), event_data);

    when_true_trace(check_and_reset_internal_dm_update(p_user_object, event_data),
                    exit, INFO, "internal update, nothing to do");

    fill_user_data_from_dm(&userdata, p_user_object);
    when_true_trace(userdata_is_empty(userdata), exit, ERROR, "User data is empty");
    SAH_TRACEZ_INFO(ME, "Update sysconf for user: %s", userdata.username);
    when_false_trace(sysconf_edit_user(&userdata) == 0, exit, ERROR, "Failed to edit User on system");

    when_failed_trace(dm_transaction_open(&user_tansaction, p_user_object), exit, ERROR, "Failed to initialize transaction");
    access_role_manage_user_bus_role(&userdata);
    when_false(dm_overload_role_empty_fields(&user_tansaction, p_user_object, userdata.username), exit);
    when_failed_trace(dm_transaction_apply(&user_tansaction), exit, ERROR, "Update transaction failed for user %s", userdata.username);

exit:
    dm_transaction_close(&user_tansaction);
    clean_userdata(&userdata);
    SAH_TRACEZ_OUT(ME);
}

void _edit_user_enable(const char* const event_name,
                       const amxc_var_t* event_data,
                       UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    userdata_t userdata = {0};
    amxd_object_t* p_user_object = NULL;

    TRACE_I_EVENT("edit_user_enable", event_name);

    init_userdata(&userdata);
    if(strcmp(event_name, OBJECT_CHANGED_EVENT_NAME) != 0) {
        TRACE_W_WKOE(OBJECT_CHANGED_EVENT_NAME, event_name);
        goto exit;
    }

    p_user_object = amxd_dm_signal_get_object(usermanagement_get_dm(), event_data);

    when_true_trace(check_and_reset_internal_dm_update(p_user_object, event_data),
                    exit, INFO, "internal update, nothing to do");

    fill_user_data_from_dm(&userdata, p_user_object);
    when_true_trace(userdata_is_empty(userdata), exit, ERROR, "User data is empty");
    SAH_TRACEZ_INFO(ME, "Update sysconf for user: %s", userdata.username);
    when_false_trace(sysconf_edit_user(&userdata) == 0, exit, ERROR, "Failed to edit User on system");
exit:
    clean_userdata(&userdata);
    SAH_TRACEZ_OUT(ME);
}

void _edit_user_group_participation_on_system(const char* const event_name,
                                              const amxc_var_t* event_data,
                                              UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    userdata_t userdata = {0};
    amxd_object_t* p_user_object = NULL;
    amxd_trans_t user_tansaction;

    TRACE_I_EVENT("edit_user_group_participation", event_name);

    amxd_trans_init(&user_tansaction);
    init_userdata(&userdata);

    if(strcmp(event_name, OBJECT_CHANGED_EVENT_NAME) != 0) {
        TRACE_W_WKOE(OBJECT_CHANGED_EVENT_NAME, event_name);
        goto exit;
    }

    p_user_object = amxd_dm_signal_get_object(usermanagement_get_dm(), event_data);

    when_true_trace(check_and_reset_internal_dm_update(p_user_object, event_data),
                    exit, INFO, "internal update, nothing to do");

    fill_user_data_from_dm(&userdata, p_user_object);
    fill_userdata_groups(&userdata, GETP_ARG(event_data, "parameters.GroupParticipation.to"));

    when_true_trace(userdata_is_empty(userdata), exit, ERROR, "User data is empty");
    SAH_TRACEZ_INFO(ME, "Update sysconf for user: %s.", userdata.username);
    when_false_trace(sysconf_edit_user(&userdata) == 0, exit, ERROR, "Failed to edit User on system");

    when_failed_trace(dm_transaction_open(&user_tansaction, p_user_object), exit, ERROR, "Failed to initialize transaction");
    when_false(dm_overload_group_empty_fields(&user_tansaction, p_user_object, userdata.username), exit);
    when_failed_trace(dm_transaction_apply(&user_tansaction), exit, ERROR, "Update transaction failed for user %s", userdata.username);

exit:
    dm_transaction_close(&user_tansaction);
    clean_userdata(&userdata);
    SAH_TRACEZ_OUT(ME);
}

void _edit_user_id_on_system(const char* const event_name,
                             const amxc_var_t* event_data,
                             UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    userdata_t userdata = {0};
    amxd_object_t* p_user_object = NULL;
    int32_t rv = 0;
    const amxc_var_t* userid_var = NULL;

    TRACE_I_EVENT("_edit_user_id_on_system", event_name);

    init_userdata(&userdata);

    if(strcmp(event_name, OBJECT_CHANGED_EVENT_NAME) != 0) {
        TRACE_W_WKOE(OBJECT_CHANGED_EVENT_NAME, event_name);
        goto exit;
    }

    p_user_object = amxd_dm_signal_get_object(usermanagement_get_dm(), event_data);
    when_true_trace(check_and_reset_internal_dm_update(p_user_object, event_data),
                    exit, INFO, "internal update, nothing to do");

    fill_user_data_from_dm(&userdata, p_user_object);

    userid_var = GETP_ARG(event_data, "parameters.UserID");
    userdata.user_id = GET_UINT32(userid_var, "to");
    SAH_TRACEZ_INFO(ME, "UserID change from: %u / to: %u", GET_UINT32(userid_var, "from"), userdata.user_id);

    SAH_TRACEZ_INFO(ME, "Update sysconf uid for %s", userdata.username);
    when_false_trace(sysconf_edit_user(&userdata) == 0, exit, ERROR, "Failed to edit User on system");

    // now update the home dir
    when_str_empty(userdata.home_dir, exit);
    when_true(is_shared_path(userdata.home_dir), exit);

    rv = fs_chown_directory(userdata.home_dir, &userdata);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to chown home dir [%s] for %s", userdata.home_dir, userdata.username);
    }

exit:
    clean_userdata(&userdata);
    SAH_TRACEZ_OUT(ME);
    return;
}

void _edit_user_name_on_system(const char* const event_name,
                               const amxc_var_t* event_data,
                               UNUSED void* const priv) {

    SAH_TRACEZ_IN(ME);
    userdata_t userdata = {0};
    amxd_object_t* p_user_object = NULL;
    amxd_trans_t user_tansaction;
    const amxc_var_t* username_var = NULL;
    bool update_needed = false;

    TRACE_I_EVENT("edit_user_name_on_system", event_name);

    amxd_trans_init(&user_tansaction);
    init_userdata(&userdata);

    if(strcmp(event_name, OBJECT_CHANGED_EVENT_NAME) != 0) {
        TRACE_W_WKOE(OBJECT_CHANGED_EVENT_NAME, event_name);
        goto exit;
    }

    p_user_object = amxd_dm_signal_get_object(usermanagement_get_dm(), event_data);
    when_true_trace(check_and_reset_internal_dm_update(p_user_object, event_data),
                    exit, INFO, "internal update, nothing to do");

    fill_user_data_from_dm(&userdata, p_user_object);
    username_var = GETP_ARG(event_data, "parameters.Username");
    userdata.username = GET_CHAR(username_var, "to");
    userdata.previous_username = GET_CHAR(username_var, "from");

    if(STRING_EMPTY(userdata.username)) {
        SAH_TRACEZ_INFO(ME, "Username provided is empty, use the alias");
        userdata.username = GET_CHAR(amxd_object_get_param_value(p_user_object, "Alias"), NULL);
        when_null_trace(userdata.username, exit, ERROR, "Internal error, alias is empty");
        update_needed = true;
    }

    SAH_TRACEZ_INFO(ME, "Update sysconf for user: %s (previous = %s)", userdata.username, userdata.previous_username);
    when_false_trace(sysconf_edit_user(&userdata) == 0, exit, ERROR, "Failed to edit User on system");

    if(update_needed) {
        when_failed_trace(dm_transaction_open(&user_tansaction, p_user_object), exit, ERROR, "Failed to initialize transaction");
        dm_overload_user_name(&user_tansaction, &userdata);
        when_failed_trace(dm_transaction_apply(&user_tansaction), exit, ERROR, "Update transaction failed for user %s", userdata.username);
    }

    access_role_remove_json_file(userdata.previous_username);
    access_role_manage_user_bus_role(&userdata);

exit:
    dm_transaction_close(&user_tansaction);
    clean_userdata(&userdata);
    SAH_TRACEZ_OUT(ME);
    return;
}

void _edit_user_shell_on_system(const char* const event_name,
                                const amxc_var_t* event_data,
                                UNUSED void* const priv) {

    SAH_TRACEZ_IN(ME);
    userdata_t userdata = {0};
    amxd_object_t* p_user_object = NULL;
    amxd_trans_t user_tansaction;


    TRACE_I_EVENT("edit_user_shell_on_system", event_name);

    amxd_trans_init(&user_tansaction);
    init_userdata(&userdata);

    if(strcmp(event_name, OBJECT_CHANGED_EVENT_NAME) != 0) {
        TRACE_W_WKOE(OBJECT_CHANGED_EVENT_NAME, event_name);
        goto exit;
    }

    p_user_object = amxd_dm_signal_get_object(usermanagement_get_dm(), event_data);
    when_true_trace(check_and_reset_internal_dm_update(p_user_object, event_data),
                    exit, INFO, "internal update, nothing to do");

    fill_user_data_from_dm(&userdata, p_user_object);
    when_failed_trace(fill_userdata_shell(&userdata, GETP_ARG(event_data, "parameters.Shell.to")),
                      exit, ERROR, "Failed to initialize transaction");

    SAH_TRACEZ_INFO(ME, "Update sysconf for user: %s", userdata.username);
    when_false_trace(sysconf_edit_user(&userdata) == 0, exit, ERROR, "Failed to edit User on system");

    when_failed_trace(dm_transaction_open(&user_tansaction, p_user_object), exit, ERROR, "Failed to initialize transaction");
    when_false(dm_overload_shell_empty_fields(&user_tansaction, p_user_object, userdata.username), exit);
    when_failed_trace(dm_transaction_apply(&user_tansaction), exit, ERROR, "Update transaction failed for user %s", userdata.username);

exit:
    dm_transaction_close(&user_tansaction);
    clean_userdata(&userdata);
    SAH_TRACEZ_OUT(ME);
    return;
}

void _edit_user_home_directory_on_system(UNUSED const char* const event_name,
                                         UNUSED const amxc_var_t* event_data,
                                         UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    userdata_t userdata = {0};
    amxd_object_t* p_user_object = NULL;
    const amxc_var_t* home_directory_var = NULL;
    const char* from = NULL;
    const char* to = NULL;
    amxc_string_t home_directory_path;

    TRACE_I_EVENT("edit_user_home_directory_on_system", event_name);

    amxc_string_init(&home_directory_path, 0);
    amxc_string_setf(&home_directory_path, "parameters.%s", (char*) get_home_directory_path());

    init_userdata(&userdata);

    if(strcmp(event_name, OBJECT_CHANGED_EVENT_NAME) != 0) {
        TRACE_W_WKOE(OBJECT_CHANGED_EVENT_NAME, event_name);
        goto exit;
    }

    p_user_object = amxd_dm_signal_get_object(usermanagement_get_dm(), event_data);
    when_true_trace(check_and_reset_internal_dm_update(p_user_object, event_data),
                    exit, INFO, "internal update, nothing to do");

    fill_user_data_from_dm(&userdata, p_user_object);

    home_directory_var = GETP_ARG(event_data, amxc_string_get(&home_directory_path, 0));
    from = GET_CHAR(home_directory_var, "from");
    to = GET_CHAR(home_directory_var, "to");

    if(!STRING_EMPTY(from) && fs_check_directory_permissions(from, &userdata)) {
        fs_remove_directory(from, &userdata);
    }

    if(!STRING_EMPTY(to) && fs_check_directory_permissions(to, &userdata)) {
        when_failed(fs_create_owned_directory(to, &userdata), exit);
    }

    userdata.home_dir = to;
    SAH_TRACEZ_INFO(ME, "Update sysconf for for user: %s", userdata.username);
    when_false_trace(sysconf_edit_user(&userdata) == 0, exit, ERROR, "Failed to edit User on system");

exit:
    amxc_string_clean(&home_directory_path);
    clean_userdata(&userdata);
    SAH_TRACEZ_OUT(ME);
}

void _edit_user_password_on_system(const char* const event_name,
                                   const amxc_var_t* event_data,
                                   UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    userdata_t userdata = {0};
    amxd_object_t* p_user_object = NULL;
    amxd_trans_t user_tansaction;

    TRACE_I_EVENT("edit_user_password_on_system", event_name);

    amxd_trans_init(&user_tansaction);
    init_userdata(&userdata);

    if(strcmp(event_name, OBJECT_CHANGED_EVENT_NAME) != 0) {
        TRACE_W_WKOE(OBJECT_CHANGED_EVENT_NAME, event_name);
        goto exit;
    }

    p_user_object = amxd_dm_signal_get_object(usermanagement_get_dm(), event_data);
    when_true_trace(check_and_reset_internal_dm_update(p_user_object, event_data),
                    exit, INFO, "internal update, nothing to do");

    // fill user data based on current data model state
    fill_user_data_from_dm(&userdata, p_user_object);

    when_failed_trace(fill_userdata_password_on_edit(&userdata, event_data), exit, ERROR, "Failed parse given password");
    when_true_trace(userdata_is_empty(userdata), exit, ERROR, "User data is empty");

    SAH_TRACEZ_INFO(ME, "Update sysconf for for user: %s", userdata.username);
    when_false_trace(sysconf_edit_user(&userdata) == 0, exit, ERROR, "Failed to edit User on system");

    when_failed_trace(dm_transaction_open(&user_tansaction, p_user_object), exit, ERROR, "Failed to initialize transaction");
    SAH_TRACEZ_INFO(ME, "Update data model password for %s.", userdata.username);
    when_failed_trace(dm_update_user_password(&user_tansaction, userdata.username, userdata.password, userdata.hashed_password),
                      exit, ERROR, "Failed to update password in data model");
    when_failed_trace(dm_transaction_apply(&user_tansaction), exit, ERROR, "Update transaction failed for user %s", userdata.username);

exit:
    dm_transaction_close(&user_tansaction);
    clean_userdata(&userdata);
    SAH_TRACEZ_OUT(ME);
    return;
}

void _edit_user_fallback(UNUSED const char* const event_name,
                         const amxc_var_t* event_data,
                         UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* p_user_object = NULL;

    TRACE_I_EVENT("edit_user_fallback", event_name);

    p_user_object = amxd_dm_signal_get_object(usermanagement_get_dm(), event_data);
    when_true_trace(check_and_reset_internal_dm_update(p_user_object, event_data),
                    exit, INFO, "internal update, nothing to do");

exit:
    SAH_TRACEZ_OUT(ME);
}

void _del_user_from_system(const char* const event_name,
                           const amxc_var_t* event_data,
                           UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    userdata_t userdata = {0};
    const amxc_var_t* instance_to_delete = NULL;
    const amxc_var_t* p_home_dir = NULL;

    TRACE_I_EVENT("del_user_from_system", event_name);

    if(strcmp(event_name, INSTANCE_REMOVED_EVENT_NAME) != 0) {
        TRACE_W_WKOE(INSTANCE_REMOVED_EVENT_NAME, event_name);
        goto exit;
    }

    init_userdata(&userdata);
    instance_to_delete = GET_ARG(event_data, "parameters");
    if(fill_userdata_id(&userdata, instance_to_delete) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to fill user data");
        goto exit;
    }

    if(sysconf_del_user(&userdata) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to delete User from system");
    }

    p_home_dir = GET_ARG(instance_to_delete, get_home_directory_path());
    fill_userdata_homedir(&userdata, p_home_dir);

    if(!STRING_EMPTY(userdata.home_dir) && fs_check_directory_permissions(userdata.home_dir, &userdata)) {
        fs_remove_directory(userdata.home_dir, &userdata);
    }
    access_role_remove_json_file(userdata.username);

    clean_userdata(&userdata);
exit:
    SAH_TRACEZ_OUT(ME);
}

void _edit_group_name_on_system(const char* const event_name,
                                const amxc_var_t* event_data,
                                UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    groupdata_t groupdata = {0};
    amxd_object_t* p_group_object = NULL;
    const amxc_var_t* group_var = NULL;

    TRACE_I_EVENT("_edit_group_name_on_system", event_name);

    init_groupdata(&groupdata);

    if(strcmp(event_name, OBJECT_CHANGED_EVENT_NAME) != 0) {
        TRACE_W_WKOE(OBJECT_CHANGED_EVENT_NAME, event_name);
        goto exit;
    }

    p_group_object = amxd_dm_signal_get_object(usermanagement_get_dm(), event_data);
    when_true_trace(check_and_reset_internal_dm_update(p_group_object, event_data),
                    exit, INFO, "internal update, nothing to do");

    fill_groupdata_from_dm(&groupdata, p_group_object);
    group_var = GETP_ARG(event_data, "parameters.Groupname");
    groupdata.groupname = GET_CHAR(group_var, "to");
    groupdata.previous_groupname = GET_CHAR(group_var, "from");

    if(!STRING_EMPTY(groupdata.previous_groupname)) {
        SAH_TRACEZ_INFO(ME, "Groupname change from: %s to: %s", groupdata.previous_groupname, groupdata.groupname);
    }

    SAH_TRACEZ_INFO(ME, "Update sysconf for group: %s", groupdata.groupname);
    when_failed_trace(sysconf_edit_group(&groupdata), exit, ERROR, "Failed to edit Group on system");

exit:
    clean_groupdata(&groupdata);
    SAH_TRACEZ_OUT(ME);
    return;
}

static amxd_status_t update_group_user_members(const char* group_path) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* p_user_object = amxd_dm_findf(usermanagement_get_dm(), "Users.User.");

    when_null_trace(group_path, exit, ERROR, "group_path is NULL");

    amxd_object_for_each(instance, it, p_user_object) {
        amxd_object_t* instance = amxc_container_of(it, amxd_object_t, it);
        const char* group_list = GET_CHAR(amxd_object_get_param_value(instance, DM_GROUP_PARTICIPATION_NAME), NULL);
        if(STRING_EMPTY(group_list)) {
            continue;
        }

        if(strstr(group_list, group_path) != NULL) {
            userdata_t userdata = {0};
            init_userdata(&userdata);
            fill_user_data_from_dm(&userdata, instance);
            SAH_TRACEZ_INFO(ME, "Update sysconf for user: %s", userdata.username);
            sysconf_edit_user(&userdata);
            clean_userdata(&userdata);
        }
    }

    status = amxd_status_ok;

exit:
    return status;
}

void _edit_group_id_on_system(const char* const event_name,
                              const amxc_var_t* event_data,
                              UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    groupdata_t groupdata = {0};
    amxd_object_t* p_group_object = NULL;
    char* group_path = NULL;

    TRACE_I_EVENT("_edit_group_id_on_system", event_name);

    init_groupdata(&groupdata);

    if(strcmp(event_name, OBJECT_CHANGED_EVENT_NAME) != 0) {
        TRACE_W_WKOE(OBJECT_CHANGED_EVENT_NAME, event_name);
        goto exit;
    }

    p_group_object = amxd_dm_signal_get_object(usermanagement_get_dm(), event_data);
    when_true_trace(check_and_reset_internal_dm_update(p_group_object, event_data),
                    exit, INFO, "internal update, nothing to do");

    group_path = amxd_object_get_path(p_group_object, AMXD_OBJECT_NAMED);
    when_null(group_path, exit);

    fill_groupdata_from_dm(&groupdata, p_group_object);
    groupdata.group_id = GETP_UINT32(event_data, "parameters.GroupID.to");

    SAH_TRACEZ_INFO(ME, "Update sysconf for group: %s", groupdata.groupname);
    when_failed_trace(sysconf_edit_group(&groupdata), exit, ERROR, "Failed to edit Group on system");

    update_group_user_members(group_path);

exit:
    free(group_path);
    clean_groupdata(&groupdata);
    SAH_TRACEZ_OUT(ME);
    return;
}

void _edit_group_role_participation_on_system(const char* const event_name,
                                              const amxc_var_t* event_data,
                                              UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    groupdata_t groupdata = {0};
    amxd_object_t* p_group_object = NULL;
    amxd_trans_t group_tansaction;

    TRACE_I_EVENT("edit_group_role_participation_on_system", event_name);

    amxd_trans_init(&group_tansaction);
    init_groupdata(&groupdata);

    if(strcmp(event_name, OBJECT_CHANGED_EVENT_NAME) != 0) {
        TRACE_W_WKOE(OBJECT_CHANGED_EVENT_NAME, event_name);
        goto exit;
    }

    p_group_object = amxd_dm_signal_get_object(usermanagement_get_dm(), event_data);

    when_true_trace(check_and_reset_internal_dm_update(p_group_object, event_data),
                    exit, INFO, "internal update, nothing to do");

    fill_groupdata_from_dm(&groupdata, p_group_object);
    fill_groupdata_role_paths(&groupdata, GETP_ARG(event_data, "parameters.RoleParticipation.to"));


    SAH_TRACEZ_INFO(ME, "Update sysconf for group: %s", groupdata.groupname);
    when_failed_trace(sysconf_edit_group(&groupdata), exit, ERROR, "Failed to edit User on system");

    when_failed_trace(dm_transaction_open(&group_tansaction, p_group_object), exit, ERROR, "Failed to initialize transaction");
    when_false(dm_overload_role_empty_fields(&group_tansaction, p_group_object, groupdata.groupname), exit);
    when_failed_trace(dm_transaction_apply(&group_tansaction) != 0, exit, ERROR, "Update transaction failed for group %s", groupdata.groupname);

exit:
    dm_transaction_close(&group_tansaction);
    clean_groupdata(&groupdata);
    SAH_TRACEZ_OUT(ME);
}

void _del_group_from_system(const char* const event_name,
                            const amxc_var_t* event_data,
                            UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    groupdata_t groupdata = {0};

    TRACE_I_EVENT("del_group_from_system", event_name);

    init_groupdata(&groupdata);

    if(strcmp(event_name, INSTANCE_REMOVED_EVENT_NAME) != 0) {
        TRACE_W_WKOE(INSTANCE_REMOVED_EVENT_NAME, event_name);
        goto exit;
    }

    fill_groupdata(&groupdata, event_data);
    if(sysconf_del_group(&groupdata) != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to delete Group from system");
    }

exit:
    clean_groupdata(&groupdata);
    SAH_TRACEZ_OUT(ME);
}
