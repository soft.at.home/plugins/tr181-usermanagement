/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "grp_entry.h"

#include <stdlib.h>
#include <grp.h>
#include <errno.h>
#include <string.h>

#include "passwd_hash.h"
#include "usermanagement_util.h"

#define ME "group"

static char** strlist_dup(char** orig) {
    SAH_TRACEZ_IN(ME);
    size_t list_len = 0;
    char** duplicate = NULL;

    for(; orig[list_len] != NULL; list_len++) {
    }

    duplicate = (char**) calloc(list_len + 1, sizeof(char*));

    for(size_t i = 0; i < list_len; i++) {
        duplicate[i] = strdup(orig[i]);
    }

    SAH_TRACEZ_OUT(ME);
    return duplicate;
}

static void strlist_del(char** to_delete) {
    SAH_TRACEZ_IN(ME);
    if(to_delete == NULL) {
        return;
    }
    for(size_t i = 0; to_delete[i] != NULL; i++) {
        free(to_delete[i]);
    }
    free(to_delete);
    SAH_TRACEZ_OUT(ME);
}

static void grp_entry_set_grent(grp_entry_t* p_this,
                                group_sys_t* p_new_entry,
                                bool should_be_freed) {
    SAH_TRACEZ_IN(ME);
    when_null_trace(p_this, exit, ERROR, "No group entry provided");
    if(p_this->m_free_grent && (p_this->mp_grent != NULL)) {
        strlist_del(p_this->mp_grent->gr_mem);
        free(p_this->mp_grent->gr_name);
        free(p_this->mp_grent->gr_passwd);
        free(p_this->mp_grent);
    }
    p_this->mp_grent = p_new_entry;
    p_this->m_free_grent = should_be_freed;

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static int32_t grp_entry_entry_get_next(db_entry_t* p_this, bool* p_entry_found) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    grp_entry_t* grp_entry = (grp_entry_t*) p_this;

    when_null_trace(p_entry_found, exit, ERROR, "No boolean variable provided");
    *p_entry_found = false;
    errno = 0;
    when_null_trace(grp_entry, exit, ERROR, "No group entry found");
    grp_entry_set_grent(grp_entry, getgrent(), false);
    when_true_trace(grp_entry->mp_grent == NULL && errno != 0,
                    exit,
                    ERROR,
                    "Failed to get next group database entry: %s",
                    strerror(errno));
    if(grp_entry->mp_grent != NULL) {
        *p_entry_found = true;
    }
    retval = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static int32_t grp_entry_entry_put(db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);

    int32_t retval = -1;
    grp_entry_t* grp_entry = (grp_entry_t*) p_this;

    when_null_trace(grp_entry, exit, ERROR, "No group entry found");
    when_failed_trace(putgrent(grp_entry->mp_grent, grp_entry->m_db_entry.mp_output_file),
                      exit,
                      ERROR,
                      "Failed to save group entry to file: %s",
                      strerror(errno));
    retval = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static bool grp_entry_entry_matches(const db_entry_t* p_this, const amxc_var_t* p_data) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    grp_entry_t* grp_entry = (grp_entry_t*) p_this;
    uint32_t gid = GET_UINT32(p_data, GROUPID_NAME);
    const char* groupname = GET_CHAR(p_data, GROUPNAME_NAME);
    const char* const previous_groupname = GET_CHAR(p_data, PREVIOUS_GROUPNAME_NAME);

    when_null_trace(grp_entry, exit, ERROR, "No group entry found");
    when_null_trace(grp_entry->mp_grent, exit, ERROR, "No mp_grent found");
    when_null_trace(grp_entry->mp_grent->gr_name, exit, ERROR, "No group name found");

    //!\ when called by sysconf_update_user_groups() only the gid is filled
    // since it is an interaction driven by user dm modifications and not group directly.
    if(grp_entry->mp_grent->gr_gid == gid) {
        rv = true;
        goto exit;
    }

    when_null_trace(grp_entry->mp_grent->gr_name, exit, ERROR, "Group name for entry should not be NULL");
    if(((groupname != NULL) && (strcmp(grp_entry->mp_grent->gr_name, groupname) == 0)) ||
       ((previous_groupname != NULL) && (strcmp(grp_entry->mp_grent->gr_name, previous_groupname) == 0))) {
        rv = true;
        if(!STRING_EMPTY(previous_groupname)) {
            SAH_TRACEZ_INFO(ME, "Groupname change from %s to %s", previous_groupname, groupname);
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int32_t grp_entry_entry_update(db_entry_t* p_this, const amxc_var_t* p_data) {
    SAH_TRACEZ_IN(ME);
    int32_t rv = -1;
    grp_entry_t* const grp_entry = (grp_entry_t*) p_this;
    group_sys_t* new_group = NULL;
    uint32_t new_group_id = GET_UINT32(p_data, GROUPID_NAME);
    const char* const new_group_name = GET_CHAR(p_data, GROUPNAME_NAME);
    const amxc_var_t* new_group_memlist_var = amxc_var_get_key(p_data,
                                                               GROUPMEMLIST_NAME,
                                                               AMXC_VAR_FLAG_DEFAULT);
    const amxc_llist_t* const new_group_memlist = amxc_var_constcast(amxc_llist_t,
                                                                     new_group_memlist_var);
    const char* const password = GET_CHAR(p_data, GROUPPASSWD_NAME);

    when_null_trace(grp_entry, exit, ERROR, "No group entry found");
    when_null_trace(grp_entry->mp_grent, exit, ERROR, "No mp_grent found in group entry");
    new_group = (group_sys_t*) calloc(1, sizeof(group_sys_t));
    when_null_trace(new_group, exit, ERROR, "Failed to allocate new group");

    new_group->gr_gid = new_group_id;
    new_group->gr_name = strdup(new_group_name);

    if(new_group_memlist != NULL) {
        new_group->gr_mem = memlist_from_amxc_llist(new_group_memlist);
    } else {
        new_group->gr_mem = strlist_dup(grp_entry->mp_grent->gr_mem);
    }

    if(password != NULL) {
        new_group->gr_passwd = strdup(generate_password_hash(new_group_name, password));
    } else {
        new_group->gr_passwd = strdup(grp_entry->mp_grent->gr_passwd);
    }
    grp_entry_set_grent(grp_entry, new_group, true);
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int32_t grp_entry_entry_create_new(db_entry_t* p_this, const amxc_var_t* p_data) {
    SAH_TRACEZ_IN(ME);
    grp_entry_t* const grp_entry = (grp_entry_t*) p_this;
    group_sys_t* new_group = NULL;
    uint32_t new_group_id = GET_UINT32(p_data, GROUPID_NAME);
    const char* const new_group_name = GET_CHAR(p_data, GROUPNAME_NAME);
    const amxc_var_t* new_group_memlist_var = amxc_var_get_key(p_data,
                                                               GROUPMEMLIST_NAME,
                                                               AMXC_VAR_FLAG_DEFAULT);
    const amxc_llist_t* const new_group_memlist = amxc_var_constcast(amxc_llist_t,
                                                                     new_group_memlist_var);
    const char* const password = GET_CHAR(p_data, GROUPPASSWD_NAME);

    when_null_trace(grp_entry, exit, ERROR, "No group entry found")
    new_group = (group_sys_t*) calloc(1, sizeof(group_sys_t));
    when_null_trace(new_group, exit, ERROR, "Failed to allocate new group");

    new_group->gr_gid = new_group_id;
    new_group->gr_name = strdup(new_group_name);

    if(new_group_memlist != NULL) {
        new_group->gr_mem = memlist_from_amxc_llist(new_group_memlist);
    } else {
        new_group->gr_mem = NULL;
    }

    if(password != NULL) {
        new_group->gr_passwd = strdup(generate_password_hash(new_group_name, password));
    } else {
        new_group->gr_passwd = strdup("x");
    }

    grp_entry_set_grent(grp_entry, new_group, true);

exit:
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static int32_t grp_entry_stream_open(UNUSED db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);

    setgrent();

    SAH_TRACEZ_OUT(ME);
    return 0;
}

static int32_t grp_entry_stream_close(UNUSED db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);

    endgrent();

    SAH_TRACEZ_OUT(ME);
    return 0;
}

static int32_t grp_entry_stream_reset(UNUSED db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);

    setgrent();

    SAH_TRACEZ_OUT(ME);
    return 0;
}

static void grp_entry_clean(db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);
    grp_entry_t* grp_entry = (grp_entry_t*) p_this;

    db_entry_clean(p_this);

    endgrent();
    grp_entry_set_grent(grp_entry, NULL, false);
    SAH_TRACEZ_OUT(ME);
}

static const vtable_t grp_vtable = {
    .entry_get_next = &grp_entry_entry_get_next,
    .entry_put = &grp_entry_entry_put,
    .entry_matches = &grp_entry_entry_matches,
    .entry_update = &grp_entry_entry_update,
    .entry_create = &grp_entry_entry_create_new,
    .stream_open = &grp_entry_stream_open,
    .stream_close = &grp_entry_stream_close,
    .stream_reset = &grp_entry_stream_reset,
    .clean = &grp_entry_clean
};

static int32_t grp_entry_init(grp_entry_t* p_this, const char* const p_filename) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;

    when_null_trace(p_this, exit, ERROR, "No group entry found");
    when_failed_trace(db_entry_init((db_entry_t*) p_this, p_filename),
                      exit,
                      ERROR,
                      "Failed to initialize database entry");

    p_this->mp_grent = NULL;
    p_this->m_free_grent = false;

    p_this->m_db_entry.mp_vtable = &grp_vtable;

    retval = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

grp_entry_t* grp_entry_new(const char* const p_filename) {
    SAH_TRACEZ_IN(ME);
    grp_entry_t* new_entry = (grp_entry_t*) calloc(1, sizeof(grp_entry_t));

    when_null_oom(new_entry, exit, "new_entry");

    if(grp_entry_init(new_entry, p_filename) != 0) {
        free(new_entry);
        new_entry = NULL;
        SAH_TRACEZ_ERROR(ME, "Failed to initialize new group entry");
        goto exit;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return new_entry;
}
