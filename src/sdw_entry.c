/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "sdw_entry.h"

#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include <debug/user_trace.h>

#include "passwd_hash.h"
#include "dm_usermanagement.h"

#define ME "shadow"

#define SECONDS_PER_DAY (60L * 60L * 24L);
#define MIN_DAYS_BEFORE_CHANGE 0
#define MAX_DAYS_BEFORE_CHANGE 99999
#define DAYS_BEFORE_WARN 7
#define DAYS_BEFORE_INACTIVE -1
#define DAYS_BEFORE_DISABLE -1
#define RESERVED -1

#define PASSWD_INVALID_STR "\x7f"

static void sdw_entry_set_spent(sdw_entry_t* p_this,
                                shadow_sys_t* new_spent,
                                bool should_be_freed) {
    SAH_TRACEZ_IN(ME);
    when_null_trace(p_this, exit, ERROR, "No shadow database entry found");
    if(p_this->m_free_spent && (p_this->mp_spent != NULL)) {
        free(p_this->mp_spent->sp_namp);
        free(p_this->mp_spent->sp_pwdp);
        free(p_this->mp_spent);
    }
    p_this->mp_spent = new_spent;
    p_this->m_free_spent = should_be_freed;
exit:
    SAH_TRACEZ_OUT(ME);
}

static int32_t sdw_entry_entry_get_next(db_entry_t* p_this, bool* p_entry_found) {
    SAH_TRACEZ_IN(ME);
    sdw_entry_t* sdw_entry = (sdw_entry_t*) p_this;

    when_null_trace(p_entry_found, exit, ERROR, "No boolean variable provided");
    *p_entry_found = false;
    when_null_trace(sdw_entry, exit, ERROR, "No shadow database entry found");

    sdw_entry_set_spent(sdw_entry, fgetspent(sdw_entry->mp_shadow_file), false);

    if(sdw_entry->mp_spent != NULL) {
        *p_entry_found = true;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return 0;
}

static int32_t sdw_entry_entry_put(db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;
    sdw_entry_t* sdw_entry = (sdw_entry_t*) p_this;

    when_null_trace(sdw_entry, exit, ERROR, "No shadow database entry found");
    when_failed_trace(putspent(sdw_entry->mp_spent, sdw_entry->m_db_entry.mp_output_file),
                      exit,
                      ERROR,
                      "Failed to store shadow database entry: %s",
                      strerror(errno));
    retval = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

static bool sdw_entry_entry_matches(const db_entry_t* p_this,
                                    const amxc_var_t* p_data) {
    SAH_TRACEZ_IN(ME);
    bool rv = false;
    sdw_entry_t* sdw_entry = (sdw_entry_t*) p_this;
    const char* const username = GET_CHAR(p_data, USERNAME_NAME);
    const char* const previous_username = GET_CHAR(p_data, PREVIOUS_USERNAME_NAME);

    when_null_trace(sdw_entry, exit, ERROR, "No shadow database entry found");
    when_null_trace(sdw_entry->mp_spent, exit, ERROR, "No shadow structure found");
    when_null_trace(sdw_entry->mp_spent->sp_namp, exit, ERROR, "No login name found");

    if(((username != NULL) && (strcmp(sdw_entry->mp_spent->sp_namp, username) == 0)) ||
       ((previous_username != NULL) && (strcmp(sdw_entry->mp_spent->sp_namp, previous_username) == 0))) {
        rv = true;
        if(!STRING_EMPTY(previous_username)) {
            SAH_TRACEZ_INFO(ME, "Username change from %s to %s", previous_username, username);
        }
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void sdw_entry_set_default_shadow_values(shadow_sys_t* to_fill) {
    when_null_trace(to_fill, exit, ERROR, "No shadow structure found");
    to_fill->sp_lstchg = time(NULL) / SECONDS_PER_DAY;
    to_fill->sp_min = MIN_DAYS_BEFORE_CHANGE;
    to_fill->sp_max = MAX_DAYS_BEFORE_CHANGE;
    to_fill->sp_warn = DAYS_BEFORE_WARN;
    to_fill->sp_inact = DAYS_BEFORE_INACTIVE;
    to_fill->sp_expire = DAYS_BEFORE_DISABLE;
    to_fill->sp_flag = RESERVED;
exit:
    return;
}

static bool is_disable_user(const char* password) {

    if(password == NULL) {
        return false;
    }

    return (*password == PASSWORD_DISABLE_STR[0]);
}

static int32_t sdw_entry_entry_update(db_entry_t* p_this,
                                      const amxc_var_t* p_data) {
    SAH_TRACEZ_IN(ME);
    sdw_entry_t* sdw_entry = (sdw_entry_t*) p_this;
    shadow_sys_t* new_entry = NULL;
    const char* const username = GET_CHAR(p_data, USERNAME_NAME);
    const char* const password = GET_CHAR(p_data, PASSWORD_NAME);
    bool user_enable = GET_BOOL(p_data, ENABLE_NAME);

    when_null_trace(sdw_entry, exit, ERROR, "No shadow database entry found");
    when_null_trace(username, exit, ERROR, "username given is NULL");

    new_entry = (shadow_sys_t*) calloc(1, sizeof(shadow_sys_t));
    when_null_oom(new_entry, exit_and_free, "new_entry");

    sdw_entry_set_default_shadow_values(new_entry);
    new_entry->sp_namp = strdup(username);
    when_null_oom(new_entry->sp_namp, exit_and_free, "username");

    if(password == NULL) {
        new_entry->sp_pwdp = strdup(PASSWORD_LOCKED_STR);
    } else if(user_enable || is_disable_user(password)) {
        new_entry->sp_pwdp = strdup(password);
    } else {
        size_t total_len = strlen(PASSWORD_DISABLE_STR) + strlen(password) + 1;
        new_entry->sp_pwdp = (char*) calloc(total_len, sizeof(char));
        if(new_entry->sp_pwdp != NULL) {
            snprintf(new_entry->sp_pwdp, total_len, "%s%s", PASSWORD_DISABLE_STR, password);
        }
    }

    when_null_oom(new_entry->sp_pwdp, exit_and_free, "password");
    sdw_entry_set_spent(sdw_entry, new_entry, true);

exit:
    SAH_TRACEZ_OUT(ME);
    return 0;

exit_and_free:
    if(new_entry != NULL) {
        free(new_entry->sp_namp);
        free(new_entry->sp_pwdp);
        free(new_entry);
    }
    SAH_TRACEZ_OUT(ME);
    return -1;
}

static int32_t sdw_entry_entry_create_new(db_entry_t* p_this,
                                          const amxc_var_t* p_data) {
    SAH_TRACEZ_IN(ME);
    sdw_entry_t* sdw_entry = (sdw_entry_t*) p_this;
    shadow_sys_t* new_entry = NULL;
    const char* const username = GET_CHAR(p_data, USERNAME_NAME);
    const char* const password = GET_CHAR(p_data, PASSWORD_NAME);
    bool user_enable = GET_BOOL(p_data, ENABLE_NAME);

    when_null_trace(sdw_entry, exit, ERROR, "No shadow database entry found");
    when_null_trace(username, exit, ERROR, "username given is NULL");

    new_entry = (shadow_sys_t*) calloc(1, sizeof(shadow_sys_t));
    when_null_oom(new_entry, exit_and_free, "new_entry");

    sdw_entry_set_default_shadow_values(new_entry);
    new_entry->sp_namp = strdup(username);
    if(password == NULL) {
        new_entry->sp_pwdp = strdup(PASSWORD_LOCKED_STR);
    } else if(user_enable || is_disable_user(password)) {
        new_entry->sp_pwdp = strdup(password);
    } else {
        size_t total_len = strlen(password) + strlen(PASSWORD_DISABLE_STR) + 1;
        new_entry->sp_pwdp = (char*) calloc(total_len, sizeof(char));
        if(new_entry->sp_pwdp != NULL) {
            snprintf(new_entry->sp_pwdp, total_len, "%s%s", PASSWORD_DISABLE_STR, password);
        }
    }

    when_null_oom(new_entry->sp_pwdp, exit_and_free, "password");
    sdw_entry_set_spent(sdw_entry, new_entry, true);

exit:
    SAH_TRACEZ_OUT(ME);
    return 0;

exit_and_free:
    if(new_entry != NULL) {
        free(new_entry->sp_namp);
        free(new_entry->sp_pwdp);
        free(new_entry);
    }
    SAH_TRACEZ_OUT(ME);
    return -1;
}

static int32_t sdw_entry_stream_open(db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    sdw_entry_t* p_sdw_entry = (sdw_entry_t*) p_this;

    when_null_trace(p_sdw_entry, exit, ERROR, "No shadow database entry found");

    when_not_null_status(p_sdw_entry->mp_shadow_file, exit, rv = 0);

    p_sdw_entry->mp_shadow_file = fopen(SHADOW_FILENAME, "r");
    fseek(p_sdw_entry->mp_shadow_file, 0, SEEK_SET);

    when_null_trace(p_sdw_entry->mp_shadow_file, exit, ERROR, "Failed to open shadow file stream: %s", strerror(errno));
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int32_t sdw_entry_stream_close(db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    sdw_entry_t* p_sdw_entry = (sdw_entry_t*) p_this;

    when_null_trace(p_sdw_entry, exit, ERROR, "No shadow database entry found");
    when_null_status(p_sdw_entry->mp_shadow_file, exit, rv = 0);

    fflush(p_sdw_entry->mp_shadow_file);
    fclose(p_sdw_entry->mp_shadow_file);
    p_sdw_entry->mp_shadow_file = NULL;

    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int32_t sdw_entry_stream_reset(UNUSED db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    sdw_entry_t* p_sdw_entry = (sdw_entry_t*) p_this;

    when_null_trace(p_sdw_entry, exit, ERROR, "No shadow database entry found");
    when_null_status(p_sdw_entry->mp_shadow_file, exit, rv = 0);

    fseek(p_sdw_entry->mp_shadow_file, 0, SEEK_SET);
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static void sdw_entry_clean(db_entry_t* p_this) {
    SAH_TRACEZ_IN(ME);
    sdw_entry_t* sdw_entry = (sdw_entry_t*) p_this;

    when_null_trace(sdw_entry, exit, ERROR, "No shadow database entry found");

    db_entry_clean(p_this);

    sdw_entry_stream_close(p_this);
    sdw_entry_set_spent(sdw_entry, NULL, false);

    ulckpwdf();
exit:
    SAH_TRACEZ_OUT(ME);
}

static const vtable_t sdw_vtable = {
    .entry_get_next = &sdw_entry_entry_get_next,
    .entry_put = &sdw_entry_entry_put,
    .entry_matches = &sdw_entry_entry_matches,
    .entry_update = &sdw_entry_entry_update,
    .entry_create = &sdw_entry_entry_create_new,
    .stream_open = &sdw_entry_stream_open,
    .stream_close = &sdw_entry_stream_close,
    .stream_reset = &sdw_entry_stream_reset,
    .clean = &sdw_entry_clean
};

static int32_t sdw_entry_init(sdw_entry_t* p_this, const char* const p_filename) {
    SAH_TRACEZ_IN(ME);
    int32_t retval = -1;

    when_null_trace(p_this, exit, ERROR, "No shadow database entry found");
    when_failed_trace(db_entry_init((db_entry_t*) p_this, p_filename),
                      exit,
                      ERROR,
                      "Failed to initialize database entry");

    lckpwdf();

    p_this->mp_spent = NULL;
    p_this->mp_shadow_file = NULL;
    p_this->m_db_entry.mp_vtable = &sdw_vtable;

    retval = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

sdw_entry_t* sdw_entry_new(const char* const p_filename) {
    SAH_TRACEZ_IN(ME);
    sdw_entry_t* new_entry = (sdw_entry_t*) calloc(1, sizeof(sdw_entry_t));
    when_null_oom(new_entry, exit, "new_entry");

    if(sdw_entry_init(new_entry, p_filename) != 0) {
        free(new_entry);
        new_entry = NULL;
        SAH_TRACEZ_ERROR(ME, "Failed to initialize new shadow entry");
        goto exit;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return new_entry;
}
