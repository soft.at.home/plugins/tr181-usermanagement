/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include "access_role.h"
#include "usermanagement.h"

#define ME "access_role"

static void write_to_json_file(const amxc_var_t* const var, const char* filename) {
    variant_json_t* writer = NULL;
    int retval = -1;
    int fd = -1;

    retval = amxj_writer_new(&writer, var);
    when_true_trace(retval != 0, exit, ERROR, "Failed to create json file writer, retval = %d", retval);

    fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0666);
    when_true_trace(fd == -1, exit, ERROR, "File open file %s - error 0x%8.8X", filename, errno);

    retval = amxj_write(writer, fd);
    when_true_trace(retval == 0, exit, ERROR, "Failed to write to file, retval = %d", retval);

exit:
    amxj_writer_delete(&writer);
    close(fd);
}

static int create_htable_for_json(amxc_var_t* input_var, const char* username) {
    int retval = -1;
    amxc_var_t* access = NULL;
    amxc_var_t* htable = NULL;
    amxc_var_t* list = NULL;

    amxc_var_add_key(cstring_t, input_var, "user", username);
    access = amxc_var_add_key(amxc_htable_t, input_var, "access", NULL);
    htable = amxc_var_add_key(amxc_htable_t, access, "*", NULL);
    list = amxc_var_add_key(amxc_llist_t, htable, "methods", NULL);
    amxc_var_add(cstring_t, list, "*");
    retval = 0;

    return retval;
}

void access_role_create_json_file(const char* const username) {
    amxc_string_t filename;
    amxc_var_t input_var;

    amxc_string_init(&filename, 0);
    amxc_var_init(&input_var);
    amxc_var_set_type(&input_var, AMXC_VAR_ID_HTABLE);

    amxc_string_appendf(&filename, "%s%s.json", USER_JSON_PATH, username);
    when_true_trace(access(amxc_string_get(&filename, 0), F_OK) == 0, exit, ERROR, "File '%s' exist. Nothing to do.", amxc_string_get(&filename, 0));
    when_true_trace(create_htable_for_json(&input_var, username) != 0, exit, ERROR, "Failed to create htable for json file.");
    write_to_json_file(&input_var, amxc_string_get(&filename, 0));

exit:
    amxc_var_clean(&input_var);
    amxc_string_clean(&filename);
}

void access_role_remove_json_file(const char* const username) {
    amxc_string_t filename;

    amxc_string_init(&filename, 0);

    amxc_string_appendf(&filename, "%s%s.json", USER_JSON_PATH, username);
    when_true_trace(access(amxc_string_get(&filename, 0), F_OK) != 0, exit, ERROR, "File '%s' does not exist. Nothing to do.", amxc_string_get(&filename, 0));
    when_true_trace(remove(amxc_string_get(&filename, 0)) != 0, exit, ERROR, "Error removing file: %s", amxc_string_get(&filename, 0));

exit:
    amxc_string_clean(&filename);
}

void access_role_manage_user_bus_role(const userdata_t* const userdata) {
    amxc_var_t csv;
    bool bus_access_role_found = false;

    amxc_var_init(&csv);
    amxc_var_set(csv_string_t, &csv, userdata->role_paths);
    amxc_var_cast(&csv, AMXC_VAR_ID_LIST);
    amxc_var_for_each(value, &csv) {
        if(strcmp(GET_CHAR(value, NULL), BUS_ACCESS_ROLE) == 0) {
            access_role_create_json_file(userdata->username);
            bus_access_role_found = true;
            break;
        }
    }
    if(!bus_access_role_found) {
        access_role_remove_json_file(userdata->username);
    }
    amxc_var_clean(&csv);
}
