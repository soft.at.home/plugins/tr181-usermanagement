#!/bin/sh

. /usr/lib/amx/scripts/amx_init_functions.sh

name="tr181-usermanagement"
datamodel_root="Users"


case $1 in
    boot)
        [ -z ${DEFAULT_ADMIN_PASSWORD} ] && export DEFAULT_ADMIN_PASSWORD="admin"
        process_boot ${name} -D
        ;;
    start)
        [ -z ${DEFAULT_ADMIN_PASSWORD} ] && export DEFAULT_ADMIN_PASSWORD="admin"
        process_start ${name} -D
        ;;
    stop)
        process_stop ${name}
        ;;
    shutdown)
        process_shutdown ${name}
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    debuginfo)
        process_debug_info ${datamodel_root}
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|debuginfo|restart]"
        ;;
esac
